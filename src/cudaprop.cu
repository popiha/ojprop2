#include "thrust\host_vector.h"
#include "thrust\device_vector.h"
#include "thrust\extrema.h"
 
#include "cudaprop.h"
using namespace ::thrust;

#if 0
#include <Eigen\Dense>
using namespace Eigen;
#endif
 
Hello::Hello(const thrust::host_vector<unsigned long>& data)
    : m_data(data.cbegin(), data.cend())
{
}
 
unsigned long Hello::Sum()
{
    return reduce(m_data.cbegin(), m_data.cend(),
                  (unsigned long)0,
                  plus<unsigned long>());
}
 
unsigned long Hello::Max()
{
    return *max_element(m_data.cbegin(), m_data.cend());
}


#if 0
void cuda_kepler_force(VectorXd &kepf,
	const thrust::host_vector< Vector3d > &rs, 
	Vector3d &rcm1, Vector3d &rcm2,
	double m1, double m2, double G) {
		thrust::host_vector<Vector3d> accs(rs.size());
		cuda_kepler_force_func op(rcm1, rcm2, m1, m2, G);
		thrust::device_vector< Vector3d > rds(rs.cbegin(), rs.cend());
		thrust::transform(rds.begin(), rds.end(), rds.begin(), op);
		thrust::copy(rds.begin(), rds.end(), accs.begin());
		for (int i=0; i < accs.size(); i++) {
			kepf.segment<3>(3*i) = accs[i];
		}
}
#endif