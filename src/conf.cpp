#include "conf.hpp"
#include "misc.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <exception>
#include <iostream>
#include <string>
#include <set>
#include <cmath>


void Configuration::sync_ptree() {
	using boost::property_tree::ptree;

	pt.clear();
	pt.put("conf.start_year", start_year);
	pt.put("conf.end_year", end_year);
	pt.put("conf.max_timestep", max_timestep);
	pt.put("conf.min_timestep", min_timestep);
	pt.put("conf.print_progress", print_progress);
	pt.put("conf.print_interval", print_interval);
	pt.put("conf.print_disk", print_disk);
	pt.put("conf.disk_interval", disk_interval);
	pt.put("conf.disk_print_distance", disk_print_distance);
	pt.put("conf.use_TTL", use_TTL);
	pt.put("conf.use_disk_m1_PN", use_disk_m1_PN);
	pt.put("conf.use_disk_m2_PN", use_disk_m2_PN);
	pt.put("conf.disk_interaction", disk_interaction);
	pt.put("conf.disk_snapshots", disk_snapshots);
	pt.put("conf.disk_accretion", disk_accretion);
	pt.put("conf.inclination_escape", disk_inclination_escape);
	pt.put("conf.fast_disk", fast_disk);
	pt.put("conf.use_openmp", use_openmp);

	pt.put("conf.limits.cut_epsilon", cut_epsilon);
	pt.put("conf.limits.cut_max_it", cut_max_it);
	pt.put("conf.limits.pn_epsilon", pn_epsilon);
	pt.put("conf.limits.pn_max_it", pn_max_it);
	pt.put("conf.limits.bs_epsilon", bs_epsilon);
	pt.put("conf.limits.escape_radius", escape_radius);

	BOOST_FOREACH(const std::string &name, tweaks)
		pt.add("conf.compatibility", name);

	pt.put("conf.postnewtonian.PN1", PN1);
	pt.put("conf.postnewtonian.PN2", PN2);
	pt.put("conf.postnewtonian.PN2_5", PN2_5);
	pt.put("conf.postnewtonian.PN3", PN3);
	pt.put("conf.postnewtonian.PN3_5", PN3_5);
	pt.put("conf.postnewtonian.spin-orbital", PN_spinorbital);
	pt.put("conf.postnewtonian.quadrupole", PN_quadrupole);
	pt.put("conf.postnewtonian.spindot", PN_spindot);

	BOOST_FOREACH(const std::string &name, debug_modules)
		pt.add("conf.debug_modules.module", name);

	pt.put("conf.debug_file", debug_file);
	pt.put("conf.orbit_file", orbit_file);
	pt.put("conf.cut_file", cut_file);
	pt.put("conf.disk_file", disk_file);
	pt.put("conf.accretion_file", accretion_file);
	pt.put("conf.particle_file", particle_file);
	pt.put("conf.stat_file", stat_file);
	pt.put("conf.element_file", element_file);
	pt.put("conf.diag_file", diag_file);
}

void Configuration::sync_instance() {
	using boost::property_tree::ptree;

	// Read variables from property tree into class
	start_year 			= pt.get<double>("conf.start_year");
	end_year 			= pt.get<double>("conf.end_year");
	max_timestep 		= pt.get<double>("conf.max_timestep");
	min_timestep 		= pt.get<double>("conf.min_timestep");
	disk_print_distance = pt.get<double>("conf.disk_print_distance");
	print_progress 		= pt.get<bool>("conf.print_progress");
	print_interval 		= pt.get<double>("conf.print_interval");
	print_disk			= pt.get<bool>("conf.print_disk");
	disk_interval		= pt.get<double>("conf.disk_interval");
	use_TTL 			= pt.get<bool>("conf.use_TTL");
	use_disk_m1_PN 		= pt.get<bool>("conf.use_disk_m1_PN");
	use_disk_m2_PN 		= pt.get<bool>("conf.use_disk_m2_PN");
	disk_interaction 	= pt.get<bool>("conf.disk_interaction");
	disk_snapshots 		= pt.get<bool>("conf.disk_snapshots");
	disk_accretion 		= pt.get<bool>("conf.disk_accretion");
	disk_inclination_escape	= pt.get<bool>("conf.disk_inclination_escape");
	fast_disk			= pt.get<bool>("conf.fast_disk");
	use_openmp			= pt.get<bool>("conf.use_openmp");

	cut_epsilon 		= pt.get<double>("conf.limits.cut_epsilon");
	cut_max_it 			= pt.get<double>("conf.limits.cut_max_it");
	pn_epsilon 			= pt.get<double>("conf.limits.pn_epsilon");
	pn_max_it 			= pt.get<double>("conf.limits.pn_max_it");
	bs_epsilon 			= pt.get<double>("conf.limits.bs_epsilon");
	escape_radius		= pt.get<double>("conf.limits.escape_radius");

	BOOST_FOREACH(ptree::value_type &v,
			pt.get_child("conf.compatibility"))
		tweaks.insert(v.first.data());

	PN1 		= pt.get<bool>("conf.postnewtonian.PN1");
	PN2 		= pt.get<bool>("conf.postnewtonian.PN2");
	PN2_5 		= pt.get<bool>("conf.postnewtonian.PN2_5");
	PN3 		= pt.get<bool>("conf.postnewtonian.PN3");
	PN3_5 		= pt.get<bool>("conf.postnewtonian.PN3_5");
	PN_spinorbital 	= pt.get<bool>("conf.postnewtonian.spin-orbital");
	PN_quadrupole 	= pt.get<bool>("conf.postnewtonian.quadrupole");
	PN_spindot 	= pt.get<bool>("conf.postnewtonian.spindot");

	BOOST_FOREACH(ptree::value_type &v,
			pt.get_child("conf.debug_modules"))
		debug_modules.insert(v.second.data());

	debug_file 	= pt.get<std::string>("conf.debug_file");
	orbit_file 	= pt.get<std::string>("conf.orbit_file");
	cut_file 	= pt.get<std::string>("conf.cut_file");
	disk_file 	= pt.get<std::string>("conf.disk_file");
	accretion_file 	= pt.get<std::string>("conf.accretion_file");
	particle_file 	= pt.get<std::string>("conf.particle_file");
	stat_file = pt.get<std::string>("conf.stat_file");
	element_file = pt.get<std::string>("conf.element_file");
	diag_file = pt.get<std::string>("conf.diag_file");
}

//void Configuration::default_configuration() {
void Configuration::defaults() {
	start_year = 1855.89679;
	end_year = 2050.0;
	max_timestep = 0.01;
	print_interval = 0.1;
	use_TTL = false;
	disk_snapshots = true;
	disk_accretion = true;

	cut_epsilon = 1e-10;
	cut_max_it = 1000;
	pn_epsilon = 1e-10;
	pn_max_it = 1000;

	debug_file.assign("debug.log");
	debug_modules.insert("main");
}

bool Configuration::tweak_enabled(const std::string &term) {
	return (tweaks.find(term) != tweaks.end());
}

bool Configuration::debug_enabled(const std::string &module_name) {
	return (debug_modules.find(module_name) != debug_modules.end());
}


