#ifdef USE_SDL
#include "globals.hpp"

#ifdef _WIN32
#define WINDOWS_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL.h>

#include <map>

#define window_width  640
#define window_height 480

// Keydown booleans
std::map<int,bool> keys;

// Poll all events
void poll_events() {
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type) {
		case SDL_KEYDOWN : keys[event.key.keysym.sym]=true ;   break;
		case SDL_KEYUP   : keys[event.key.keysym.sym ]=false;   break;
		}
	}
}

// Initialze OpenGL perspective matrix
void GL_Setup(int width, int height)
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glEnable( GL_DEPTH_TEST );
	gluPerspective( 45, (float)width/height, 0.1, 100 );
	glMatrixMode( GL_MODELVIEW );
}

void sdl_init() {
	// Initialize SDL with best video mode
	SDL_Init(SDL_INIT_VIDEO);
	const SDL_VideoInfo* info = SDL_GetVideoInfo();
	int vidFlags = SDL_OPENGL | SDL_GL_DOUBLEBUFFER;
	if (info->hw_available) {vidFlags |= SDL_HWSURFACE;}
	else {vidFlags |= SDL_SWSURFACE;}
	int bpp = info->vfmt->BitsPerPixel;
	SDL_SetVideoMode(window_width, window_height, bpp, vidFlags);
	GL_Setup(window_width, window_height);

	// Some GL props
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(5);
}

void sdl_graphics_loop() {
	// Poll keys
	poll_events();

	// Draw
	static float yaw = 0, pitch = 0;
	static const float scale = 10000;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0,0, -10);
	glRotatef(-90-pitch, 1, 0, 0);
	glRotatef(yaw, 0, 0, 1);
	glBegin(GL_POINTS);

	// Draw binary
	Vector3d v1 = g_state.m_rel1*g_state.bin.r;
	Vector3d v2 = -g_state.m_rel2*g_state.bin.r;
	glColor3ub(0, 255, 0);
	glVertex3d(v1[0]/scale, v1[1]/scale, v1[2]/scale);
	glColor3ub(0, 255, 0);
	glVertex3d(v2[0]/scale, v2[1]/scale, v2[2]/scale);

	// Draw disk
	for (int i=0; i < g_state.disk.n; i++) {
		Vector3d &v = g_state.disk.rs[i];
		switch (g_state.disk.states[i]) {
			case disk::NORMAL:
				glColor3ub(255, 0, 0); 
				break;
			default:
				glColor3ub(0,0,255);
		}
		glVertex3d(v[0]/scale, v[1]/scale, v[2]/scale);
	}

	// 
	glEnd();
	SDL_GL_SwapBuffers();
	// Check keypresses
	if (keys[SDLK_RIGHT]){ yaw-=0.5; }
	if (keys[SDLK_LEFT ]){ yaw+=0.5; }
	if (keys[SDLK_UP   ]){ pitch-=0.5; }
	if (keys[SDLK_DOWN ]){ pitch+=0.5; }
}
#endif