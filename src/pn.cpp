#include "pn.hpp"
#include "conf.hpp"
#include "units.hpp"
#include "globals.hpp"

#include <cmath>
#include <iostream>

void pn_do_precalcs(pn_precalcs &p, double m1, double m2, double chi, double q) {
	p.m = m1+m2; 
	p.m1 = m1;
	p.gm = C_G*p.m;
	p.g3 = C_G*C_G*C_G;

	p.eta = m1*m2/(p.m*p.m); 
	p.eta2 = p.eta*p.eta; 
	p.eta3 = p.eta2*p.eta;
	p.sq14eta = sqrt(1-4*p.eta);

	p.c2 = C_c*C_c; 
	p.c3 = C_c*p.c2;
	p.c4 = p.c2*p.c2; 
	p.c5 = C_c*p.c4; 
	p.c6 = p.c2*p.c4; 
	p.c7 = C_c*p.c6;

	p.chi = chi;
	p.chi2 = chi*chi;
	p.q = q;

	p.pi2 = M_PI*M_PI;
}

Vector3d pn_terms(pn_precalcs &p, const Vector3d &rv, const Vector3d &vv) {
	// From Valtonen/Mikkola code, based on
	// Mora & Will, Phys. Rev. D 69, 104021
	double r = rv.norm();
	double gmpr = p.gm/r, gmpr2 = gmpr*gmpr, gmpr3 = gmpr2*gmpr;
	double vr = rv.dot(vv)/r;
	double vr2 = vr*vr;
	double vr4 = vr2*vr2;
	double vr6 = vr2*vr4;
	double v2 = vv.dot(vv), v4 = v2*v2, v6 = v4*v2;
	Vector3d n = rv/r;
	double A1=0, A2=0, A2p5=0, A3=0, A3p5=0;
	double B1=0, B2=0, B2p5=0, B3=0, B3p5=0;

	debug_print(stderr, "X", rv);
	debug_print(stderr, "V", vv);
	debug_print(stderr, "r", r);
	debug_print(stderr, "rdotv", rv.dot(vv));
	debug_print(stderr, "vr", vr);
	debug_print(stderr, "n", n);
	debug_print(stderr, "m", p.m);
	debug_print(stderr, "eta", p.eta);

	if (g_progconf.PN1) {
		A1=2*(2+p.eta)*gmpr-(1+3*p.eta)*v2 +1.5e0*p.eta*vr2;
		B1=2*(2-p.eta)*vr;
	}

	if (g_progconf.PN2) {
		A2=-.75e0*(12+29*p.eta)*gmpr2 -
			p.eta*(3-4*p.eta)*v2*v2
			-15.e0/8*p.eta*(1-3*p.eta)*vr4
			+.5e0*p.eta*(13-4*p.eta)*gmpr*v2
			+(2+25*p.eta+2*p.eta2)*gmpr*vr2
			+1.5e0*p.eta*(3-4*p.eta)*v2*vr2;
		B2=-.5e0*vr*((4+41*p.eta+8*p.eta2)*gmpr-p.eta*(15+4*p.eta)*v2
				+3*p.eta*(3+2*p.eta)*vr2);
	}

	if (g_progconf.PN2_5) {
		A2p5=8.e0/5*p.eta*gmpr*vr*(17.e0/3*gmpr+3*v2);
		B2p5=-8./5.*p.eta*gmpr*(3*gmpr+v2);
	}

	if (g_progconf.PN3) {
		A3=(16+(1399./12-41./16*p.pi2)*p.eta+71./2*p.eta2)*gmpr3
		+p.eta*(20827./840+123./64*p.pi2-p.eta2)*gmpr2*v2
		-(1+(22717./168+615./64*p.pi2)*p.eta+11./8*p.eta2-7*p.eta3)
		*gmpr2*vr2
		-.25e0*p.eta*(11-49*p.eta+52*p.eta2)*v6
		+35./16*p.eta*(1-5*p.eta+5*p.eta2)*vr6
		-.25e0*p.eta*(75+32*p.eta-40*p.eta2)*gmpr*v2*v2
		-.5e0*p.eta*(158-69*p.eta-60*p.eta2)*gmpr*vr4
		+p.eta*(121-16*p.eta-20*p.eta2)*gmpr*v2*vr2
		+3./8*p.eta*(20-79*p.eta+60*p.eta2)*v2*v2*vr2
		-15./8*p.eta*(4-18*p.eta+17*p.eta2)*v2*vr4;
		B3=vr*((4+(5849./840.+123./32.*p.pi2)*p.eta
				-25*p.eta2-8*p.eta3)*gmpr2
			+1./8.*p.eta*(65-152*p.eta-48*p.eta2)*v2*v2
			+15/8.*p.eta*(3-8*p.eta-2*p.eta2)*vr4
			+p.eta*(15+27*p.eta+10*p.eta2)*gmpr*v2
			-1./6.*p.eta*(329+177*p.eta+108*p.eta2)*gmpr*vr2
			-.75*p.eta*(16-37*p.eta-16*p.eta2)*v2*vr2);
	}

	if (g_progconf.PN3_5) {
		A3p5=-8./5*p.eta*gmpr*vr*(23./14*(43+14*p.eta)*gmpr2
			+3./28*(61+70*p.eta)*v2*v2
			+70*vr4+1./42*(519-1267*p.eta)*gmpr*v2
			+.25e0*(147+188*p.eta)*gmpr*vr2
			-15/4.*(19+2*p.eta)*v2*vr2);
		B3p5=8./5.*p.eta*gmpr*(1./42.*(1325+546*p.eta)*gmpr2
			+1./28.*(313+42*p.eta)*v2*v2+75*vr4
			-1./42.*(205+777*p.eta)*gmpr*v2
			+1./12.*(205+424*p.eta)*gmpr*vr2
			-.75*(113+2*p.eta)*v2*vr2);
	}
	 
	double Atot=A1/p.c2 + A2/p.c4 + A2p5/p.c5 + A3/p.c6 + A3p5/p.c7;
	double Btot=B1/p.c2 + B2/p.c4 + B2p5/p.c5 + B3/p.c6 + B3p5/p.c7;

	// DEBUG:
	debug_print(stderr, "A1", A1);
	debug_print(stderr, "A2", A2);
	debug_print(stderr, "A2p5", A2p5);
	debug_print(stderr, "A3", A3);
	debug_print(stderr, "B1", B1);
	debug_print(stderr, "B2", B2);
	debug_print(stderr, "B2p5", B2p5);
	debug_print(stderr, "B3", B3);

	return p.gm/(r*r)*(n*Atot + vv*Btot);
}

Vector3d pn_spin_terms(pn_precalcs &p,
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s) {
	double r = rv.norm(), r2 = r*r, r3 = r2*r, r4 = r2*r2;
	double rd = rv.dot(vv)/r;
	double nbrac[2] = {0,0};
	//double nsbrac=0, vsbrac=0;
	Vector3d SO(0,0,0), Q(0,0,0);

	/* nv = rhat */
	Vector3d nv = rv.normalized();

	/* ns = nv x s */
	Vector3d ns = nv.cross(s);

	/* vs = vv x s */
	Vector3d vs = vv.cross(s);

	/* nvvv = nv x vv */
	Vector3d nvvv = nv.cross(vv);

	/* nvs = nv.s */
	double nvs = nv.dot(s);

	/* SO */
	if (g_progconf.PN_spinorbital) {
		nbrac[0] = 12 * s.dot(nvvv);
		double nsbrac = (9+3*p.sq14eta)*rd;
		double vsbrac = 7 + p.sq14eta;
		// XXX: minus or no minus? No minus.
		SO = p.gm*p.gm/(r3*p.c3)*(1+p.sq14eta)/4 * p.chi
			* (nbrac[0]*nv + nsbrac*ns - vsbrac*vs);
	}

	/* Q */
	double Qcoeff=0;
	if (g_progconf.PN_quadrupole) {
		nbrac[1] = 5 * nvs*nvs - 1;
		// XXX: minus or no minus? Minus. Differs from the
		// paper, matches with Valtonen/Mikkola code

		//Qcoeff = -p.q*p.chi*p.chi*3*p.g3*p.m1*p.m1*p.m/(2*p.c4*r4);

		// Mikkola code assumes m1 >> m2 => m ~ m1
		Qcoeff = -p.q*p.chi*p.chi*3*p.g3*p.m1*p.m1*p.m1/(2*p.c4*r4);
		Q = Qcoeff * (nbrac[1]*nv - 2*nvs*s);
	}

	// DEBUG
	debug_print(stderr, "c", C_c);
	debug_print(stderr, "sqrt(c2)", sqrt(p.c2));
	debug_print(stderr, "Qcoeff", Qcoeff);
	debug_print(stderr, "qpara", p.q);
	debug_print(stderr, "j", s);
	debug_print(stderr, "n", nv);
	debug_print(stderr, "nvs", nvs);
	debug_print(stderr, "SO", SO);
	debug_print(stderr, "Q", Q);
	debug_print(stderr, "SO+Q", SO+Q);

	return SO + Q;
}


Vector3d pn_sdot(pn_precalcs &p,
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s) {
	double r = rv.norm();

	if (!g_progconf.PN_spindot)
		return Vector3d::Zero();

	Vector3d omega = rv.cross(vv)/r;
	omega *= p.gm*p.eta/(2*p.c2*r*r)*(7+p.sq14eta)/(1+p.sq14eta);
	Vector3d retval = omega.cross(s);

	// DEBUG
	double chi = 0.27543739760606223;
	debug_print(stderr, "omega", omega);
	debug_print(stderr, "alpha", s);
	debug_print(stderr, "dspin", retval);
	debug_print(stderr, "alpha*chi", (s*chi));
	debug_print(stderr, "dspin*chi", (retval*chi));

	return retval;
}

// Without precalcs
#if 1
Vector3d pn_terms(double gc, double c, double m1, double m2, 
		const Vector3d &rv, const Vector3d &vv) {
	// TODO: As much as possible should be precalculated

	// From Valtonen/Mikkola code, based on
	// Mora & Will, Phys. Rev. D 69, 104021
	const double m = m1+m2, eta = m1*m2/(m*m), gm = gc*m;
	const double c2 = c*c, c4 = c2*c2, c5 = c*c4, c6 = c2*c4, c7=c*c6;
	const double eta2 = eta*eta, eta3 = eta2*eta;
	const double r = rv.norm();
	const double gmpr = gm/r, gmpr2 = gmpr*gmpr, gmpr3 = gmpr2*gmpr;
	const double vr = rv.dot(vv)/r;
	const double vr2 = vr*vr;
	const double vr4 = vr2*vr2;
	const double vr6 = vr2*vr4;
	const double v2 = vv.dot(vv), v4 = v2*v2, v6 = v4*v2;
	const Vector3d n = rv/r;
	const double pi2 = M_PI*M_PI;
	double A1=0, A2=0, A2p5=0, A3=0, A3p5=0;
	double B1=0, B2=0, B2p5=0, B3=0, B3p5=0;

	// TODO: These should all be precalculated.
	debug_print(stderr, "X", rv);
	debug_print(stderr, "V", vv);
	debug_print(stderr, "r", r);
	debug_print(stderr, "rdotv", rv.dot(vv));
	debug_print(stderr, "vr", vr);
	debug_print(stderr, "n", n);
	debug_print(stderr, "m", m);
	debug_print(stderr, "eta", eta);

	//if (g_progconf.pn_term_enabled("PN1"))
	if (g_progconf.PN1) {
		A1=2*(2+eta)*gmpr-(1+3*eta)*v2 +1.5e0*eta*vr2;
		B1=2*(2-eta)*vr;
	}

	if (g_progconf.PN2) {
		A2=-.75e0*(12+29*eta)*gmpr2 -
			eta*(3-4*eta)*v2*v2
			-15.e0/8*eta*(1-3*eta)*vr4
			+.5e0*eta*(13-4*eta)*gmpr*v2
			+(2+25*eta+2*eta2)*gmpr*vr2
			+1.5e0*eta*(3-4*eta)*v2*vr2;
		B2=-.5e0*vr*((4+41*eta+8*eta2)*gmpr-eta*(15+4*eta)*v2
				+3*eta*(3+2*eta)*vr2);
	}

	if (g_progconf.PN2_5) {
		A2p5=8.e0/5*eta*gmpr*vr*(17.e0/3*gmpr+3*v2);
		B2p5=-8./5.*eta*gmpr*(3*gmpr+v2);
	}

	if (g_progconf.PN3) {
		A3=(16+(1399./12-41./16*pi2)*eta+71./2*eta2)*gmpr3
		+eta*(20827./840+123./64*pi2-eta2)*gmpr2*v2
		-(1+(22717./168+615./64*pi2)*eta+11./8*eta2-7*eta3)
		*gmpr2*vr2
		-.25e0*eta*(11-49*eta+52*eta2)*v6
		+35./16*eta*(1-5*eta+5*eta2)*vr6
		-.25e0*eta*(75+32*eta-40*eta2)*gmpr*v2*v2
		-.5e0*eta*(158-69*eta-60*eta2)*gmpr*vr4
		+eta*(121-16*eta-20*eta2)*gmpr*v2*vr2
		+3./8*eta*(20-79*eta+60*eta2)*v2*v2*vr2
		-15./8*eta*(4-18*eta+17*eta2)*v2*vr4;
		B3=vr*((4+(5849./840.+123./32.*pi2)*eta
				-25*eta2-8*eta3)*gmpr2
			+1./8.*eta*(65-152*eta-48*eta2)*v2*v2
			+15/8.*eta*(3-8*eta-2*eta2)*vr4
			+eta*(15+27*eta+10*eta2)*gmpr*v2
			-1./6.*eta*(329+177*eta+108*eta2)*gmpr*vr2
			-.75*eta*(16-37*eta-16*eta2)*v2*vr2);
	}

	if (g_progconf.PN3_5) {
		A3p5=-8./5*eta*gmpr*vr*(23./14*(43+14*eta)*gmpr2
			+3./28*(61+70*eta)*v2*v2
			+70*vr4+1./42*(519-1267*eta)*gmpr*v2
			+.25e0*(147+188*eta)*gmpr*vr2
			-15/4.*(19+2*eta)*v2*vr2);
		B3p5=8./5.*eta*gmpr*(1./42.*(1325+546*eta)*gmpr2
			+1./28.*(313+42*eta)*v2*v2+75*vr4
			-1./42.*(205+777*eta)*gmpr*v2
			+1./12.*(205+424*eta)*gmpr*vr2
			-.75*(113+2*eta)*v2*vr2);
	}

	// DEBUG:
	debug_print(stderr, "A1", A1);
	debug_print(stderr, "A2", A2);
	debug_print(stderr, "A2p5", A2p5);
	debug_print(stderr, "A3", A3);
	debug_print(stderr, "B1", B1);
	debug_print(stderr, "B2", B2);
	debug_print(stderr, "B2p5", B2p5);
	debug_print(stderr, "B3", B3);
	 
	const double Atot=A1/c2 + A2/c4 + A2p5/c5 + A3/c6 + A3p5/c7;
	const double Btot=B1/c2 + B2/c4 + B2p5/c5 + B3/c6 + B3p5/c7;

	return gm/(r*r)*(n*Atot + vv*Btot);
}

Vector3d pn_spin_terms(double gc, double c, double m1, double m2, 
		double chi, double q, 
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s) {
	const double m = m1+m2, eta = m1*m2/(m*m), gm = gc*m;
	const double r = rv.norm(), r2 = r*r, r3 = r2*r, r4 = r2*r2;
	const double c2 = c*c, c3 = c2*c, c4 = c2*c2;
	const double rd = rv.dot(vv)/r;
	double nbrac[2] = {0,0}, nsbrac=0, vsbrac=0;
	Vector3d SO(0,0,0), Q(0,0,0);

	/* nv = rhat */
	const Vector3d nv = rv.normalized();

	/* ns = nv x s */
	const Vector3d ns = nv.cross(s);

	/* vs = vv x s */
	const Vector3d vs = vv.cross(s);

	/* nvvv = nv x vv */
	const Vector3d nvvv = nv.cross(vv);

	/* nvs = nv.s */
	const double nvs = nv.dot(s);

	/* SO */
	if (g_progconf.PN_spinorbital) {
		nbrac[0] = 12 * s.dot(nvvv);
		nsbrac = (9+3*sqrt(1-4*eta))*rd;
		vsbrac = 7 + sqrt(1-4*eta);
		// XXX: minus or no minus? No minus.
		SO = gm*gm/(r3*c3)*(1+sqrt(1-4*eta))/4 * chi
			* (nbrac[0]*nv + nsbrac*ns - vsbrac*vs);
	}

	/* Q */
	double Qcoeff = 0;
	if (g_progconf.PN_quadrupole) {
		nbrac[1] = 5 * nvs*nvs - 1;
		// TODO: minus or no minus? Minus.
		// XXX: Mikkola code assumes m2 << m1 => m ~ m1
		Qcoeff = -q*chi*chi*3*gc*gc*gc*m1*m1*m1/(2*c4*r4);
		
#if 0
		Q = -q*chi*chi*3*gc*gc*gc*m1*m1*m/(2*c4*r4)
			* (nbrac[1]*nv - 2*nvs*s);
#else
		Q = Qcoeff * (nbrac[1]*nv - 2*nvs*s);
#endif
	}

	// DEBUG
	debug_print(stderr, "c", C_c);
	debug_print(stderr, "sqrt(c2)", sqrt(c2));
	debug_print(stderr, "Qcoeff", Qcoeff);
	debug_print(stderr, "qpara", q);
	debug_print(stderr, "j", s);
	debug_print(stderr, "n", nv);
	debug_print(stderr, "nvs", nvs);
	debug_print(stderr, "SO", SO);
	debug_print(stderr, "Q", Q);
	debug_print(stderr, "SO+Q", SO+Q);

	return SO + Q;
}


Vector3d pn_sdot(double gc, double c, double m1, double m2, 
		double chi, double q, 
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s) {
	double m = m1+m2, eta = m1*m2/(m*m), gm = gc*m;
	double r = rv.norm();

	if (!g_progconf.PN_spindot)
		return Vector3d::Zero();

	Vector3d omega = rv.cross(vv)/r;
	omega *= gm*eta/(2*c*c*r*r)*(7+sqrt(1-4*eta))/(1+sqrt(1-4*eta));
	return omega.cross(s);
}
#endif
