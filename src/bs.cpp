#include "bs.hpp"
#include "system.hpp"
#include "units.hpp"
#include "pn.hpp"
#include "propagator.hpp"
#include "globals.hpp"

#include <cstdio>
#include <cstdlib>
#include <iomanip>

#include <algorithm>

/* Bulirsch-Stoer propagation. Adapted from Press et al. 1992. */

/* helper functions to marshall to/from state */
// Number of state variables
// = 1 + 9 + N*6 + 2 = 12 + N*6
static inline int n_vars(State &s) { 
	return 3 + s.objs.n_dyn_bh * s.objs.bhs.size() + s.objs.n_dyn_part * s.objs.parts.size();
}

static inline int n_vars_bholes(State &s) { 
	return 3 + s.objs.n_dyn_bh * s.objs.bhs.size();
}

void bin2y(State &s, double *res) {
	double sw = sqrt(s.integrator_state.ttlw);
	res[0] = 0;		// Not used by the silly Numerical Recipes code

	res[1] = s.physical.t;  
	res[2] = s.integrator_state.ttlw;
	res[3] = s.constants.E_gr;

	long offset = 3;
	int stride = s.objs.n_dyn_bh;
	for (long i = 0; i < s.objs.bhs.size(); i++) {
		bhole &pi = s.objs.bhs[i];
		res[offset + 1 + i*stride] = pi.r[0];
		res[offset + 2 + i*stride] = pi.r[1];
		res[offset + 3 + i*stride] = pi.r[2];
		res[offset + 4 + i*stride] = pi.v[0]/sw;
		res[offset + 5 + i*stride] = pi.v[1]/sw;
		res[offset + 6 + i*stride] = pi.v[2]/sw;
		res[offset + 7 + i*stride] = pi.s[0];
		res[offset + 8 + i*stride] = pi.s[1];
		res[offset + 9 + i*stride] = pi.s[2];
	}

	offset += s.objs.n_dyn_bh * s.objs.bhs.size(); 
	stride = s.objs.n_dyn_part;
	for (long i=0; i < s.objs.parts.size(); i++) {
		particle &pi = s.objs.parts[i];
		res[offset + 1 + i*stride]	= pi.r[0];
		res[offset + 2 + i*stride] 	= pi.r[1];
		res[offset + 3 + i*stride] 	= pi.r[2];
		res[offset + 4 + i*stride] 	= pi.v[0] /sw;
		res[offset + 5 + i*stride]	= pi.v[1] /sw;
		res[offset + 6 + i*stride]	= pi.v[2] /sw;
	}
}

void y2bin(double *y, State &s) {
	s.physical.t			= y[1];
	s.integrator_state.ttlw = y[2];
	s.constants.E_gr		= y[3];

	double sw = sqrt(s.integrator_state.ttlw);

	long offset = 3;
	int stride = s.objs.n_dyn_bh;
	for (long i = 0; i < s.objs.bhs.size(); i++) {
		bhole &pi = s.objs.bhs[i];
		pi.r[0] = y[offset + 1 + i*stride];
		pi.r[1] = y[offset + 2 + i*stride];
		pi.r[2] = y[offset + 3 + i*stride];
		pi.v[0] = y[offset + 4 + i*stride] * sw;
		pi.v[1] = y[offset + 5 + i*stride] * sw;
		pi.v[2] = y[offset + 6 + i*stride] * sw;
		pi.s[0] = y[offset + 7 + i*stride];
		pi.s[1] = y[offset + 8 + i*stride];
		pi.s[2] = y[offset + 9 + i*stride];
	}

	offset += s.objs.n_dyn_bh * s.objs.bhs.size();
	stride = s.objs.n_dyn_part;
	for (long i=0; i < s.objs.parts.size(); i++) {
		particle &pi = s.objs.parts[i];
		pi.r[0] = y[offset + i*stride + 1]; 
		pi.r[1] = y[offset + i*stride + 2]; 			
		pi.r[2] = y[offset + i*stride + 3]; 			
		pi.v[0] = y[offset + i*stride + 4] * sw; 		
		pi.v[1] = y[offset + i*stride + 5] * sw; 	
		pi.v[2] = y[offset + i*stride + 6] * sw; 	
	}
}




/* helper function to perform a series of lf2 steps */
void bssubsteps(const State &sin, double h, int nsteps, double out[]) {
	/* size of substep */
	h /= nsteps;
	// TODO: Duplicating the state takes a lot of memory for many
	// particles. Do not duplicate the whole state?
	/* copy state */
	State s = sin;
	//memcpy(&c, &s, sizeof(state));

	//double hs = (g_progconf.use_TTL ? h/s.integrator_state.ttlw : h);
	// DEBUG:
	debug_print(stderr, "nsteps", nsteps);
	debug_print(stderr, "h", h);
	debug_print(stderr, "W", s.integrator_state.ttlw);

	// r-leap
	//rleap(s, hs/2);
	rleap(s, h/2);

	// Alternate v and r leaps
	for (int n = 0; n < nsteps; n++) {
		// v-leap
		vleap(s, h);

		// Recalculate step and check for final half-step
		/*
		hs = (g_progconf.use_TTL ? h/s.integrator_state.ttlw : h);
		if (n == nsteps-1) hs /= 2;
		*/
		if (n == nsteps-1) h /= 2;

		// r-leap
		//rleap(s, hs);
		rleap(s, h);
	}
	
	// marshall state into output
	bin2y(s, out);
}

/* various NR macros re-implemented */
#define SQR(x) ((x)*(x))
static inline double FMAX(double a, double b) { return (a > b ? a : b); }
static inline double FMIN(double a, double b) { return (a < b ? a : b); }

/* NR memory allocation */
static double* nr_vector(int start, int num) {

	double *a;
	if ((a = (double*)malloc((num+start)*sizeof(double))) == NULL) {
		fprintf(stderr, "malloc: failed\n");
		exit(EXIT_FAILURE);
	}
	return a;
}

static double** nr_matrix(int sr, int nr, int sc, int nc) {
	double **a;
	int i;
	if ((a = (double**)malloc((nr+sr)*sizeof(double*))) == NULL) {
		fprintf(stderr, "malloc: failed\n");
		exit(EXIT_FAILURE);
	}
	for (i = sr; i <= nr; i++) {
		if ((a[i] = (double*)malloc((nc+sc)*sizeof(double))) == NULL) {
			fprintf(stderr, "malloc: failed\n");
			exit(EXIT_FAILURE);
		}
	}
	return a;
}

static void free_vector(double *v, int start, int num) {
	free(v);
}

static void free_matrix(double **m, int sr, int nr, int sc, int nc) {
	int i;
	for (i = sr; i <= nr; i++)
		free(m[i]);
	free(m);
}

#define KMAXX 8 /* Maximum row number used in the extrapolation. orig: 8 */
#define IMAXX (KMAXX+1) 
#define SAFE1 0.25 /* Safety factors. */
#define SAFE2 0.7
#define REDMAX 1.0e-5 /* Maximum factor for stepsize reduction. */
#define REDMIN 0.7 /* Minimum factor for stepsize reduction. */
#define TINY 1.0e-30 /* Prevents division by zero. */
/* 1/SCALMX is the maximum factor by which a stepsize can be increased. */
#define SCALMX 0.1 

/* Use polynomial extrapolation to evaluate nv functions at x = 0 by fitting a
 * polynomial to a sequence of estimates with progressively smaller values x =
 * xest, and corresponding function vectors yest[1..nv]. This call is number
 * iest in the sequence of calls. Extrapolated function values are output as
 * yz[1..nv], and their estimated error is output as dy[1..nv].
 */

/* Pointers to matrix and vector used by pzextr or rzextr. */
double **d,*x;
static void pzextr(int iest, double xest, double yest[], double yz[], 
		double dy[], int nv) {
	int k1,j;
	double q,f2,f1,delta,*c;
	c=nr_vector(1,nv);
	x[iest]=xest; /* Save current independent variable. */
	for (j=1;j<=nv;j++) dy[j]=yz[j]=yest[j];
	/* Store first estimate in first column. */
	if (iest == 1) { 
		for (j=1;j<=nv;j++) d[j][1]=yest[j];
	} 
	else {
		for (j=1;j<=nv;j++) c[j]=yest[j];
		for (k1=1;k1<iest;k1++) {
			delta=1.0/(x[iest-k1]-xest);
			f1=xest*delta;
			f2=x[iest-k1]*delta;
			for (j=1;j<=nv;j++) { 
				/* Propagate tableau 1 diagonal more. */
				q=d[j][k1];
				d[j][k1]=dy[j];
				delta=c[j]-q;
				dy[j]=f1*delta;
				c[j]=f2*delta;
				yz[j] += dy[j];
			}
		}
		for (j=1;j<=nv;j++) d[j][iest]=dy[j];
	}
	free_vector(c,1,nv);
}

void bsstep(State &s, double *xx, double htry, double eps, 
		double *hdid, double *hnext) {
	/* need indexing to start from 1, due to odd conventions in NR */
	int nv = n_vars(s);
	int nvbin = n_vars_bholes(s);
	double *y = nr_vector(1,nv);
	/*
	double yscal[NUMV+1] = { 1, 1, 1, 1, 1, 1, 1, 
				    1, 1, 1, 1, 1, 1, 1 };
				    */
	int erri; // index of maximum error
	int i,iq,k,kk,km;
	static int first=1,kmax,kopt;
	static double epsold = -1.0,xnew;
	double eps1,errmax,fact,h,red=1.0,scale=1.0,work,wrkmin,xest;
	double *err,*yerr,*ysav,*yseq;
	static double a[IMAXX+1];
	static double alf[KMAXX+1][KMAXX+1];
	// XXX: Can we include 1 step in the sequence? Try it so, leave out 18
	//static int nseq[IMAXX+1]={0,2,4,6,8,10,12,14,16,18};
	static int nseq[IMAXX+1]={0,1,2,4,6,8,10,12,14,16};
	int reduct,exitflag=0;

	/* marshal state to table y */
	bin2y(s, y);

	d=nr_matrix(1,nv,1,KMAXX);
	err=nr_vector(1,KMAXX);
	x=nr_vector(1,KMAXX);
	yerr=nr_vector(1,nv);
	ysav=nr_vector(1,nv);
	yseq=nr_vector(1,nv);
	if (eps != epsold) { /* A new tolerance, so reinitialize. */
		*hnext = xnew = -1.0e29; /*  "Impossible" values. */
		eps1=SAFE1*eps;
		a[1]=nseq[1]+1; /* Compute work coefficients Ak. */
		for (k=1;k<=KMAXX;k++) a[k+1]=a[k]+nseq[k+1];
		for (iq=2;iq<=KMAXX;iq++) { /* Compute alpha(k; q). */
			for (k=1;k<iq;k++)
				alf[k][iq]=pow(eps1,(a[k+1]-a[iq+1])/
						((a[iq+1]-a[1]+1.0)*(2*k+1)));
		}
		epsold=eps;
		/* Determine optimal row number for convergence. */
		for (kopt=2;kopt<KMAXX;kopt++) 
			if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt]) break; 
		kmax=kopt;
	}
	h=htry;
	for (i=1;i<=nv;i++) ysav[i]=y[i]; /* Save the starting values.*/
	if (*xx != xnew || h != (*hnext)) { 
		/* A new stepsize or a new integration: re-establish the order
		 * window. */
		first=1; 
		kopt=kmax;
	}
	reduct=0;
	for (;;) {
		for (k=1;k<=kmax;k++) { 
			/* Evaluate the sequence of modified midpoint
			 * integrations. */
			xnew=(*xx)+h; 
			/* check that there are no fictious or real time
			 * underflows, though these only happen when
			 * using automatic step size control. */
			if (xnew == (*xx)) {
				fprintf(stderr, 
				"bsstep: step size underflow\n");
				exit(EXIT_FAILURE);
			}

			//bssubsteps(s, *xx, h, nseq[k], yseq);
			//fprintf(stderr, "substeps h %16.10g n %5d\n", h, nseq[k]);
			bssubsteps(s, h, nseq[k], yseq);
			if (yseq[1] == ysav[1]) {
				fprintf(stderr, 
				"bsstep: physical time underflow in bssubsteps\n");
				exit(EXIT_FAILURE);
			}
			/* Squared, since error series is even. */
			xest=SQR(h/nseq[k]); 
			/* Perform extrapolation. */
			pzextr(k,xest,yseq,y,yerr,nv); 
			if (k != 1) { 
				/* Compute normalized error estimate foo (k) */
				errmax=TINY;
				// XXX: Use constant 1 for all error
				// estimate scaling
				erri=0;
				// XXX: Only calculating error for binary part of the state!
				// TODO: THIS AFFECTS PERFORMANCE IN A MAJOR WAY! 
				//for (i=1;i<=nv;i++) {
				for (i=1;i<=nvbin;i++) {
					if (fabs(yerr[i]) > errmax) {
						// Scale with y
						// XXX: Must add a number of order BS-epsilon in the denominator
						errmax = fabs(yerr[i]/(y[i] + 10*eps));
						//errmax = fabs(yerr[i]);
						erri = i;
					}
					//errmax=FMAX(errmax, fabs(yerr[i]/yscal[i]));
				}
				/* Scale error relative to tolerance. */
				errmax /= eps; 
				km=k-1;
				err[km]=pow(errmax/SAFE1,1.0/(2*km+1));
                if (g_progconf.debug_enabled("bs")) 
					g_debug_stream << "bs: k " << k << " erri " << erri << " errmax " << errmax << endl;
                    //fprintf(stderr, "k: %d erri: %d errmax: %g\n", k, erri, errmax);
			}
			if (k != 1 && (k >= kopt-1 || first)) { 
				/* In order window. */
				if (errmax < 1.0) { 
					/* Converged. */
					exitflag=1;
					break;
				}
				if (k == kmax || k == kopt+1) { 
					/* Check for possible stepsize
					 * reduction. */
					red=SAFE2/err[km]; 
					break;
				}
				else if (k == kopt 
					&& alf[kopt-1][kopt] < err[km]) {
					red=1.0/err[km];
					break;
				}
				else if (kopt == kmax 
						&& alf[km][kmax-1] < err[km]) {
					red=alf[km][kmax-1]*SAFE2/err[km];
					break;
				}
				else if (alf[km][kopt] < err[km]) {
					red=alf[km][kopt-1]/err[km];
					break;
				}
			}
		}
		if (exitflag) break;
		/* Reduce stepsize by at least REDMIN and at most REDMAX. */
		red=std::min(red,REDMIN);
		red=std::max(red,REDMAX);
		h *= red;
		reduct=1;
	} /* Try again. */

	/* Successful step taken. */
	*xx=xnew; 
	*hdid=h;
	first=0;
	/* Compute optimal row for convergence and corresponding stepsize. */
	wrkmin=1.0e35; 
	for (kk=1;kk<=km;kk++) { 
		fact=std::max(err[kk],SCALMX); 
		work=fact*a[kk+1];
		if (work < wrkmin) {
			scale=fact;
			wrkmin=work;
			kopt=kk+1;
		}
	}
	*hnext=h/scale;
	if (kopt >= k && kopt != kmax && !reduct) {
		/* Check for possible order increase, but not if stepsize was
		 * just reduced. */
		fact=std::max(scale/alf[kopt-1][kopt], SCALMX);
		if (a[kopt+1]*fact <= wrkmin) {
			*hnext=h/fact;
			kopt++;
		}
	}

	/* marshall results back into state */
	y2bin(y, s);
	free_vector(y,1,nv);
	free_vector(yseq,1,nv);
	free_vector(ysav,1,nv);
	free_vector(yerr,1,nv);
	free_vector(x,1,KMAXX);
	free_vector(err,1,KMAXX);
	free_matrix(d,1,nv,1,KMAXX);
}
