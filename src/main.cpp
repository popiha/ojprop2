#ifdef _WIN32
#define WINDOWS_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#endif

#include "globals.hpp"
#include "conf.hpp"
#include "system.hpp"
#include "propagator.hpp"
#include "units.hpp"
#include "misc.hpp"
#include "interpolation.hpp"
#include "viscosity.hpp"

#include <omp.h>
#include <boost/foreach.hpp>
#include <boost/circular_buffer.hpp>
#include <Eigen/Core>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <csignal>
#include <map>
#include <stdlib.h>
#include <ctime>

using namespace std;

// Function prototypes
typedef void (*sighnd)(int);
void signal_handler(int sig); 
void particle_snapshot(State &s); 
void particle_snapshot(State &s, int i); 
void load_from_file(PropertyTreeContainer &cnt, std::string fname);
void print_omp_debug();
void print_eigen_debug();
//void print_mpi_debug();
int print_debug_dump();
bool detect_cut(double curr_pcsec , double prev_pcsec) { return curr_pcsec*prev_pcsec < 0; }
double poincare_section(const State &s);
void register_sighnds();
void print_output(double phys_t);

// Struct to hold values for interpolation when a cut is detected
struct interpolates {
	double phys_t;
	double r;
	double z;
	double vz;
	double psec;
	void calculate(const State &s) {
		const bhole &p1 = s.objs.bhs[0];
		const bhole &p2 = s.objs.bhs[1];
		Vector3d rr = p2.r - p1.r;
		Vector3d rv = p2.v - p1.v;
		phys_t = s.physical_time();
		r = rr.segment<2>(0).norm();
		z = rr.z();
		vz = rv.z();
		psec = poincare_section(s);
	}
};

// Length of the interpolation buffer
const int IBUF_LENGTH = 5;

void interp_cut(struct interpolates *ibuf);

int main(int argc, char **argv) {
	// Before anything else, we must make sure that Eigen is ready to handle a multithreaded environment
	Eigen::initParallel();
#pragma omp barrier

	// Initialize MPI
	//mpi_init(argc, argv, g_mpi_data);

	// Handle command line arguments
	if (argc != 2) {
		std::cerr << argv[0] << ":\n" <<
			"usage: " << argv[0] << " FILE\n" <<
			"    FILE must contain valid intial value data\n";
		exit(EXIT_FAILURE);
	}

	// Load configuration first, so that we can setup all streams, incl. debug stream
	load_from_file(g_progconf, "default.conf");

	// Open output streams
	open_streams();

	// Print disinformation
	print_omp_debug();
	print_eigen_debug();
	//print_mpi_debug();

	// Register signal handlers
	register_sighnds();

	// XXX: Valtonen/Mikkola uses a system where c = 10000 (??), G = 1.
	if (g_progconf.tweak_enabled("lightspeed"))
		C_c = 10000.0;

	// Load initial values from file given as a command line argument
	g_state.load_from_file(argv[1]);

	// Print initial conditions
	State &s = g_state;
	g_orbit_stream << s.bholes_to_string() << std::endl;
	int disk_snap=1;
	particle_snapshot(s, disk_snap++);
	

	// Print out what we read in
	if (g_progconf.debug_enabled("main")) 
		print_debug_dump();

	// Calculate timestep as a fraction of the (initial) period
	double g = C_G;
	double timestep = g_progconf.max_timestep;
	if (g_progconf.debug_enabled("main")) 
		g_debug_stream << "timestep = " << timestep << std::endl;

	// These variables tell when was the last time we output data,
	// by which means output frequency is controlled
	double last_print = g_progconf.start_year;
	double last_disk  = g_progconf.start_year;

	// Get time elapsed for distant observer and use it as a
	// reference for everything in propagation loop
	double phys_t = s.physical_time();

	// Store previous state and two values of poincare section for interpolation
	State prev_state;
	prev_state = s;
	double curr_pcsec = poincare_section(s);
	double prev_pcsec = curr_pcsec;
	// Also store z-component of secondary rcm coordinates
	double curr_rcm2z = s.objs.bhs[1].r.z();
	double prev_rcm2z = curr_rcm2z;

	// Initialize Time Transformed Leapfrog
	if (g_progconf.use_TTL) {
		// Calculate initial time transformation variable value
		s.integrator_state.ttlw = omega(s);
		if (g_progconf.debug_enabled("TTL"))
			g_debug_stream << "w = " << s.integrator_state.ttlw << std::endl;
		// If TTL is enabled, scale so that initial timesteps at
		// apocentre agree
		// DEBUG: try to match Mikkola timestepping
		//timestep *= s.ttlw;
	}

	// Initialize viscosity calculation from Lehto & Valtonen 1996
	viscosity_precalc();

	// Create a circular buffer for the interpolates
	int ibuf_index = 0;
	interpolates ibuf[IBUF_LENGTH];
	memset(&ibuf, 0, sizeof(interpolates)*IBUF_LENGTH);

	// Store time for printing out simulation duration
	std::time_t start_time;
	std::time(&start_time);

	//
	// Main propagation loop
	for (;;) {
		// Stop integration if we've reached endpoint
		if (phys_t >= g_progconf.end_year)
			break;

		// Propagate system, calculate new physical time
		propagate(s, timestep);
		phys_t = s.physical_time();

		// We wish to interpolate after a cut to find precise
		// figures for time and other interesting variables, so
		// we need to keep a buffer of those values.
		// Keep buffer ordered, store in last position always.
		memmove(ibuf, ibuf+1, sizeof(interpolates)*(IBUF_LENGTH-1));
		ibuf[IBUF_LENGTH-1].calculate(s);

		// Store new values
		curr_pcsec = poincare_section(s);
		curr_rcm2z = s.objs.bhs[1].r.z(); 

		// Output orbit and accretion data if the output interval has passed
		if (last_print + g_progconf.print_interval < phys_t) {
			print_output(phys_t); 
			last_print = phys_t;
		}

		// Output disk data if the interval has passed
		if (last_disk + g_progconf.disk_interval < phys_t) {
			particle_snapshot(s, disk_snap++);
			last_disk = phys_t;
		}

		// Find all intersections with the poincare section (== disk)
		if (detect_cut(curr_pcsec, prev_pcsec)) {
			// Cut found
			if (g_progconf.debug_enabled("main"))
				g_debug_stream << "Cut found! t = " << s.physical_time() << "\n";


			// DEBUG: don't do anything
#if 0
			// Print disk data for this cut
			particle_snapshot(s);

			interp_cut(ibuf);
#endif
		}

		// Store state in the circular buffer
		prev_state = s;
		prev_pcsec = curr_pcsec;
		prev_rcm2z = curr_rcm2z;
	}

	// Simulation done. Print statistics about the run
	{
		g_debug_stream << "Simulation done." << std::endl;
		std::cout << "Simulation done." << std::endl;
		// Time spent
		{
			std::stringstream sb;
			std::time_t end_time;
			time(&end_time);
			double sec = std::difftime(end_time, start_time);
			double min = sec/60;
			double hr = sec/3600;
			sb << "Took " << sec << " s = " << min << " m = " << hr << " h";
			g_debug_stream << sb.str() << std::endl;
			std::cout << sb.str() << std::endl;
		}
		// Particle escapes & accretions
		{
			std::stringstream sb;
			sb	<< "Particles: total / escaped / accreted:\n" << s.objs.parts.size() 
				<< " " << s.stats.escape_total << " " << s.stats.accretion_total;
			g_debug_stream << sb.str() << std::endl;
			std::cout << sb.str() << std::endl;
		}
	}
 
	// Close streams and return success
	close_streams();
	return 0;
}

// Handler to flush output streams when terminated with a signal
void signal_handler(int sig) {
	// Flush and close all streams
	flush_streams();
	close_streams();
	exit(EXIT_FAILURE);
}

// Output a snapshot of the disk particles and disk statistics
void particle_snapshot(State &s, int i) {
	// Create filename from template and current time
	std::stringstream ss;
	if (i >= 0)
		ss << i << ".out";
	else
		ss << s.physical_time() << ".out";

	std::ofstream ssfile(("disk_" + ss.str()).c_str());
	ssfile << s.parts_to_string();

#if 0
	// If we were called with index number for plotting purposes, add the orbit data
	if (i >= 0)
		ssfile << "\n\n" << s.bin_to_string();
#endif
	ssfile.close();

	// So far, no use for radial stats.
#if 0
	ssfile.open(("disk_stats_" + ss.str()).c_str());
	ssfile << s.disk_radstats_to_string();
	ssfile.close();
#endif
}

void particle_snapshot(State &s) {
	particle_snapshot(s, -1);
}

double poincare_section(const State &s) {
#if 0
	int ri, pi;
	if (!g_grid1.index2(s.bin.rcm2, ri, pi)) {
		//fprintf(stderr, "binary not within grid, rcm2.z %g\n", s.bin.rcm2[2]);
		return s.bin.rcm2[2];
	}
	double zmed = s.stats.cell[ri][pi].z_median;
	//fprintf(stderr, "bin z %g disk z %g pc %g\n", s.bin.rcm2[2], zmed, s.bin.rcm2[2] - zmed);
	return s.bin.rcm2[2] - zmed;
#endif
	return s.objs.bhs[1].r.z();
}

void interp_cut(struct interpolates *ibuf) {
	// our x-value is the value of the poincare section, y values vary
	double xbuf[IBUF_LENGTH];
	double tbuf[IBUF_LENGTH];
	double rbuf[IBUF_LENGTH];
	double zbuf[IBUF_LENGTH];
	double vzbuf[IBUF_LENGTH];

	for (int i=0; i < IBUF_LENGTH; i++) {
		xbuf[i] = ibuf[i].psec;
		tbuf[i] = ibuf[i].phys_t;
		rbuf[i] = ibuf[i].r;
		zbuf[i] = ibuf[i].z;
		vzbuf[i] = ibuf[i].vz;
	}
	if (g_progconf.debug_enabled("main")) {
		g_debug_stream << "Cut interpolation, buffer contents: (psec, t, r, z, vz)\n";
		for (int i=0; i < IBUF_LENGTH; i++) {
			g_debug_stream
				<< xbuf[i] << " "
				<< tbuf[i] << " "
				<< rbuf[i] << " "
				<< zbuf[i] << " "
				<< vzbuf[i] << "\n";
		}
	}

	// Interpolate each value
	const int order = IBUF_LENGTH-1;
	fh_precalc pc;
	fh_precalculate(&pc, xbuf, tbuf, order);
	double cut_t = fh_interp(&pc, 0);

	fh_reassign(&pc, xbuf, rbuf);
	double cut_r = fh_interp(&pc, 0);

	fh_reassign(&pc, xbuf, zbuf);
	double cut_z = fh_interp(&pc, 0);
	
	fh_reassign(&pc, xbuf, vzbuf);
	double cut_vz = fh_interp(&pc, 0);
	
	fh_free(&pc);

	g_stat_stream 
		<< std::scientific << std::setprecision(15) 
		<< cut_t << " " << cut_r << " " << cut_z << " " << cut_vz << std::endl;
}

void print_omp_debug() {
	g_debug_stream << "OMP: debugging information:\n";
	if (!g_progconf.use_openmp) {
		g_debug_stream << "OMP: OpenMP not in use." << endl;
		return;
	}
#pragma omp parallel
	{
#pragma omp critical(debugprint)
		g_debug_stream << "OMP: thread no. " << omp_get_thread_num() << " reporting\n";
#pragma omp barrier
#pragma omp master
		g_debug_stream << "OMP: number of threads " << omp_get_num_threads() << "\n";
	}
}

void print_eigen_debug() {
	g_debug_stream << "Eigen: debugging information\n";
#ifdef EIGEN_NO_DEBUG
	g_debug_stream << "Eigen: Debugging mode not enabled (FAST)" << endl;
#else
	g_debug_stream << "Eigen: Debugging mode enabled (SLOW)" << endl;
#endif
	g_debug_stream << "Eigen: Number of threads in use: " << Eigen::nbThreads() << endl;
}

//void print_mpi_debug() {
//	g_debug_stream << "MPI: debugging information\n";
//	g_debug_stream << "MPI: size " << g_mpi_data.size << endl;
//}

void load_from_file(PropertyTreeContainer &cnt, std::string fname) {
	try {
		cnt.load(fname);
	}
	catch(std::exception &e) {
		std::cerr << "Error loading " << fname << ":" << e.what() << "\n";
		std::exit(EXIT_FAILURE);
		//cnt.defaults();
	}
}

int print_debug_dump() {
	g_debug_stream << "conf:\n" << g_progconf.to_string() << "\n";
	//g_debug_stream << "state:\n" << g_state.bholes_to_string() << "\n";
	g_debug_stream << "black holes:\n";
	for (long i=0; i < g_state.objs.bhs.size(); i++) {
		g_state.objs.bhs[i].serialize_to_stream(g_debug_stream);
		g_debug_stream << "\n";
	}
	g_debug_stream << "particles :\n";
	for (long i=0; i < g_state.objs.parts.size(); i++) {
		g_state.objs.parts[i].serialize_to_stream(g_debug_stream);
		g_debug_stream << "\n";
	}
		

	g_debug_stream << "debugged modules: ";
	BOOST_FOREACH(const std::string &name, g_progconf.debug_modules)
		g_debug_stream << name << " ";
	g_debug_stream << "\n";

	g_debug_stream << "enabled PN-modules: ";
	/*
	BOOST_FOREACH(const std::string &name, g_progconf.pn_terms)
	g_debug_stream << name << " ";
	*/
	if (g_progconf.PN1) g_debug_stream << "PN1 ";
	if (g_progconf.PN2) g_debug_stream << "PN2 ";
	if (g_progconf.PN2_5) g_debug_stream << "PN2_5 ";
	if (g_progconf.PN3) g_debug_stream << "PN3 ";
	if (g_progconf.PN3_5) g_debug_stream << "PN3_5 ";
	if (g_progconf.PN_spinorbital) g_debug_stream << "spin-orbital ";
	if (g_progconf.PN_quadrupole) g_debug_stream << "quadrupole ";
	if (g_progconf.PN_spindot) g_debug_stream << "spindot ";
	g_debug_stream << "\n";

	g_debug_stream << "enabled tweaks: ";
	BOOST_FOREACH(const std::string &name, g_progconf.tweaks)
		g_debug_stream << name << " ";
	g_debug_stream << "\n";
	g_debug_stream << "Internal G = " << stringify(C_G)
		<< " c = " << stringify(C_c) << "\n";
	return 0;
}


void register_sighnds() {
	sighnd prev_sighnd;
	prev_sighnd = signal(SIGINT, signal_handler);
	if (prev_sighnd == SIG_IGN)
		signal(SIGINT, SIG_IGN);
	prev_sighnd = signal(SIGTERM, signal_handler);
	if (prev_sighnd == SIG_IGN)
		signal(SIGTERM, SIG_IGN);
}

void print_output(double phys_t) {
	State &s = g_state;

	// Output orbit data
	g_orbit_stream << s.bholes_to_string() << std::endl;

	// Output orbital element statistics
	s.update_element_stats();
	g_element_stream << s.disk_oe_stats_to_string() << std::endl;

	// Output accumulating stats
	g_accretion_stream << s.stat_accumulators_to_string() << std::endl;
	s.clear_accumulators();

	// Output diagnostics
	g_diag_stream << s.diagnostics_to_string() << std::endl;

	if (g_progconf.print_progress) {
		printf("Progress: %f%%, physical time %f\n",
			100*(phys_t-g_progconf.start_year) / (g_progconf.end_year-g_progconf.start_year),
			phys_t);
	}

	// Explicitly streams to make sure freshest data possible was written
	flush_streams();
}
