#include "viscosity.hpp"
#include "interpolation.hpp"

#include <cstdlib>
#include <cmath>

// Values derived from the article:
// 1) distance in AUs
// 2) nu in AU^2 (G=1 => time scaled)
double viscosity_table[2][VISCOSITY_POINTS] = {
	{
		3.66120031e+003, 
		5.20199841e+003, 
		6.74279651e+003, 
		8.28565727e+003, 
		9.82645537e+003, 
		1.13672535e+004, 
		1.29101142e+004, 
		1.44509123e+004, 
		1.59917104e+004, 
		1.75345712e+004, 
		1.90753693e+004
	},
	{
		9.44789342e+000,
		9.21491806e+000,
		8.93631622e+000,
		8.71579849e+000,
		8.48808407e+000,
		8.29210641e+000,
		8.11460430e+000,
		7.86925873e+000,
		7.58373697e+000,
		7.30742756e+000,
		6.92719410e+000,
	}
};

// A global interpolation table
static fh_precalc visco_precalc;

// Free data at exit
static void viscosity_free() {
	fh_free(&visco_precalc);
}

void viscosity_precalc() {
	fh_precalculate(&visco_precalc, viscosity_table[0], viscosity_table[1], VISCOSITY_POINTS);
	std::atexit(viscosity_free);
}

double viscosity_at(double r) {
	double lognu = fh_interp(&visco_precalc, r);
	return std::exp(lognu);
}