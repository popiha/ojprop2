#include "grid.hpp"
#include "conf.hpp"
#include "disk.hpp"
#include "globals.hpp"
#include "misc.hpp"

#include <iostream>
#include <fstream>
#include <math.h>

#include <Eigen/Dense>
using namespace Eigen;

void MillerGrid::initialize(int r_divs, int phi_divs, double rmax, Vector3d &pos) {
	// Set position
	position = pos;

	// Set grid parameters
	nr = r_divs;
	nphi = phi_divs;
	this->rmax = rmax;
	ncells = nr*nphi;
	dphi = 2*M_PI/nphi;
	rmin = rmax * exp(-2*M_PI*nr/nphi);
	alpha = 2*M_PI/nphi;

	g_debug_stream << "MillerGrid.generate: minimum R = " << rmin << "\n";

	// Reserve space for all gridcells
	g.reserve(nr);
	for (int i=0; i < nr; i++) {
		g.push_back(std::vector< std::vector<int> >(nphi));
		for (int j=0; j < nphi; j++) 
			g[i].push_back(std::vector<int>());
	}

	// Precalculate areas, and other useful constants
	g_debug_stream << "MillerGrid.generate: precalculating" << std::endl;
	MillerGrid::precalc();
	g_debug_stream << "MillerGrid.generate: done" << std::endl;
}

// Check if a particle is inside the grid
bool MillerGrid::inside(const Vector3d &in) {
	Vector3d v = in - position;
	double x = v[0], y = v[1];
	double r = sqrt(x*x + y*y);

	// Check for escapees
	if (r > rmax || r < rmin || !isfinite(r)) 
		return false;

	return true;
}

// Convert from cartesian coordinates to grid cell index pair. Return
// true if the particle is in the grid or false if it has escaped
bool MillerGrid::index(const Vector3d &in, int &ind) {
	Vector3d v = in - position;
	int ri, pi;
	bool rval = index2(v, ri, pi);
	if (rval)
		ind = ri*nphi + pi;
	return rval;
}

bool MillerGrid::index2(const Vector3d &in, int &r_i, int &p_i) {
	Vector3d v = in-position;
	double x = v[0], y = v[1];
	double r = sqrt(x*x + y*y);

	// Check for escapees
	if (r > rmax || r < rmin) 
		return false;

	// Not an escapee. Calculate index of radial slot, in [0,n_grid_r-1]
	r_i = (int)floor(1/alpha*log(r/rmin));

	// Calculate number of angular slot, in [0,n_grid_phi-1]
	double phi = atan2(y, x);
	if (phi < 0) phi += 2*M_PI;
	p_i = (int)floor(phi/dphi);

	return true;
}

void MillerGrid::precalc() {
	cell_areas.clear();
	cell_areas.reserve(nr);
	fprintf(stderr, "MillerGrid.precalc: alpha %g rmin %g nr %d nphi %d\n",
			alpha, rmin, nr, nphi);

	total_area = M_PI*(rmax*rmax - rmin*rmin);

	for (int i=0; i < nr; i++) {
		double area = M_PI*rmin*rmin/nphi*(exp(2*alpha)-1)*exp(2*alpha*i);
		cell_areas.push_back(area);
		fprintf(stderr,"area %g relarea %g\n", area, area/(M_PI*(rmax*rmax - rmin*rmin)));
	}
}

//void MillerGrid::update(struct disk &d) {
//void MillerGrid::update(std::vector<Vector3d>& rs, std::vector<Vector3d&> &in, std::vector<Vector3d&> &out) {
void MillerGrid::update(std::vector<Vector3d>& rs) {

	// Clear grid cells
	for (int i=0; i < nr; i++)
		for (int j=0; j < nphi; j++)
			g[i][j].clear();

	//g_in.clear();
	g_out.clear();

	// Go through points, assign to grid cells
	int ri, pi;
	int n = (int)rs.size();
	for (int i=0; i < n; i++) {
		// Check if the particle is inside the grid
		bool inside_grid = index2(rs[i], ri, pi);
		if (inside_grid) {
			g[ri][pi].push_back(i);
		}
		else {
			g_out.push_back(i);
		}
	}
}