#include "globals.hpp"
#include "misc.hpp"

#include <cmath>

#include <Eigen/Dense>
using namespace Eigen;

Vector3d pos_cart2cyl(Vector3d &r) {
	Vector3d ret;
	// radial component
	ret[0] = sqrt(r[0]*r[0]+r[1]*r[1]);
	// angular component
	ret[1] = std::atan2(r[1], r[0]);
	// axial component
	ret[2] = r[2];
	return ret;
}

Vector3d cart2lvlh(const Vector3d &r, const Vector3d &v) {
	// Construct LVLH frame, and project the velocity vector to it

	// Radial unit vector
	Vector3d rad = Vector3d::Zero();
	rad[0] = r[0];
	rad[1] = r[1];
	rad.normalize();

	// Angular unit vector, by rotating radial unit vector through a right angle
	Vector3d th = Vector3d::Zero();
	th[0] = -rad[1];
	th[1] = rad[0];

	// Axial unit vector
	Vector3d ax = Vector3d::UnitZ();

	// Project velocity components
	Vector3d ret;
	ret[0] = v.dot(rad);
	ret[1] = v.dot(th);
	ret[2] = v.dot(ax);

	return ret;
}

Vector3d lvlh2cart(const Vector3d &r, const Vector3d &v) {
	Vector3d ret;
	// Construct LVLH frame, and project the velocity vector to it

	// Radial unit vector
	Vector3d rad = Vector3d::Zero();
	rad[0] = r[0];
	rad[1] = r[1];
	rad.normalize();

	// Angular unit vector, by rotating radial unit vector through a right angle
	Vector3d th = Vector3d::Zero();
	th[0] = -rad[1];
	th[1] = rad[0];

	// Axial unit vector
	Vector3d ax = Vector3d::UnitZ();

	// Project velocity components
	return v[0]*rad + v[1]*th + v[2]*ax;
}

void rv2oe_gp(double mu, const Vector3d &rv, const Vector3d &vin, double *res) {
	//double ev[3], hv[3], rhat[3];
	double r = rv.norm();
	double alpha, eta, zeta, h, b;

	/* vv = v/mu */
	Vector3d vv = vin/sqrt(mu);

	/* alpha = 2/r - vv . vv, reciprocal of semimajor axis */
	alpha = 2/r - vv.dot(vv);
	res[0] = 1/alpha;

	/* eta = rv . vv */
	eta = rv.dot(vv); 

	/* zeta = 1 - alpha * r */
	zeta = 1 - alpha*r;

	/* hv = rv x vv, specific angular momentum */
	Vector3d hv = rv.cross(vv);
	h = hv.norm();

	/* rhat = rv / r, unit vector in r direction */
	Vector3d rhat = rv.normalized();

	/* ev = vv x hv - rhat, eccentricity vector */
	Vector3d ev = vv.cross(hv) - rhat;
	res[1] = ev.norm();

	/* length of the projection of angular momentum on xy-plane */
	b = sqrt(hv[0]*hv[0] + hv[1]*hv[1]);

	/* inclination */
	res[2] = atan2(b, hv[2]);

	/* longitude of ascending node */
	res[3] = atan2(hv[0], -hv[1]);

	/* argument of pericentre */
	res[4] = atan2(ev[2]*h, ev[1]*hv[0] - ev[0]*hv[1]);

	/* mean anomaly at epoch */
	res[5] = atan2(eta*sqrt(alpha), zeta) - eta*sqrt(alpha);
}

void rv2oe(const Vector3d &r, const Vector3d &v, double *res) {
	rv2oe_gp(1.0, r, v, res);
}

void oe2rv_gp(double mu, const double *el, Vector3d &rv, Vector3d &vv) {
	Vector3d pv, qv, wv; /* orbital plane coordinate system axes */
	Vector3d av, bv; /* semi-major & minor axes */
	double E, r, vc;

	pv[0] =  cos(el[4])*cos(el[3]) - sin(el[4])*sin(el[3])*cos(el[2]);
	pv[1] =  cos(el[4])*sin(el[3]) + sin(el[4])*cos(el[3])*cos(el[2]);
	pv[2] =  sin(el[4])*sin(el[2]);

	qv[0] = -sin(el[4])*cos(el[3]) - cos(el[4])*sin(el[3])*cos(el[2]);
	qv[1] = -sin(el[4])*sin(el[3]) + cos(el[4])*cos(el[3])*cos(el[2]);
	qv[2] =  cos(el[4])*sin(el[2]);

	wv[0] =  sin(el[3])*sin(el[2]);
	wv[1] = -cos(el[3])*sin(el[2]);
	wv[2] =  cos(el[2]);

	/* TODO: make tolerance and maxit settable */
	/* solve elliptic kepler M = E - e*sin(E) */
	if (solve_ekepler(1e-15, 10, el[1], el[5], &E) != 0) {
		std::string s("oe2rv_gp: solve_ekepler: no convergence\n");
		g_debug_stream << s;
		std::cerr << s;
	}

	/* av = a * pv */
	av = el[0] * pv;

	/* bv = sqrt(1-e^2) * wv x av */
	bv = sqrt(1-el[1]*el[1]) * wv.cross(av);

	/* rv = (cos E - e) * av + sin E * bv */
	rv = (cos(E)-el[1])*av + sin(E) * bv;
	r = rv.norm();

	/* vv = sqrt(mu/(r*sqrt(a))) * [-sin E * av + cos E * bv ] */
	vc = sqrt(mu)/(r*sqrt(el[0]));
	vv = (-vc*sin(E))*av + cos(E)*bv;
}

void oe2rv(const double *elements, Vector3d &r, Vector3d &v) {
	oe2rv_gp(1.0, elements, r, v);
}

int solve_ekepler(double tol, int maxit, double ecc, double M, double *E) {
	double f, df, ddf, x=M;
	int i=0;
	do  {
		/* use a suggestion by Seppo Mikkola */
		i++;
		//f = ad_ekepler(ecc, x) - x; 
		f = - ecc*sin(x); 
		df = 1.0 - ecc*cos(x);
		ddf = ecc*sin(x);
		x = x - f/sqrt(df*df - f*ddf);
	} while (fabs(f) > tol && i < maxit);

	*E = x;
	if (fabs(f) > tol)
		return 1;

	return 0;
}

void basic_stats(const double *values, const int n, double *out) {
	if (n==0) { 
		// No values, do nothing and silently return
		return;
	}
	// Mean, min, max
	double acc = 0;
	double min = values[0], max = values[0];
	for (int i=0; i < n; i++) {
		acc += values[i];
		if (values[i] < min)
			min = values[i];
		if (values[i] > max)
			max = values[i];
	}
	out[0] = acc/n;
	out[2] = min;
	out[3] = max;
	// Stddev
	acc = 0;
	for (int i=0; i < n; i++) {
		double x = values[i] - out[0];
		acc += x*x;
	}
	if (n == 1)
		out[1] = 0;
	else
		out[1] = sqrt(acc/(n-1));
}

void basic_stats(const std::vector<double> &values, double *out) {
	basic_stats(&values[0], (int)values.size(), out);
}


#if 0
template<typename Derived>
bool isnotnan(const ArrayBase<Derived>& x) {
	return x == x;
}

template<typename Derived>
bool isfinite(const ArrayBase<Derived>& x) {
	return isnotnan(x - x);
}
#endif

bool custom_isnotnan(const double x) { return x == x; }
bool custom_isfinite(const double x) { return custom_isnotnan(x-x); }

// Platform independent /dev/null
std::ostream cnull(0);
std::wostream wcnull(0);