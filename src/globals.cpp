//#include "mpi.hpp"
#include "globals.hpp"

// One global configuration variable for all parts of the program
Configuration g_progconf;

// Global state
State g_state;

// MPI state
//MPI_data g_mpi_data;

// Output streams
std::ofstream g_debug_stream;
std::ofstream g_orbit_stream;
std::ofstream g_cut_stream;
std::ofstream g_disk_stream;
std::ofstream g_accretion_stream;
std::ofstream g_particle_stream;
std::ofstream g_stat_stream;
std::ofstream g_element_stream;
std::ofstream g_diag_stream;

// Open all output streams
void open_streams() {
	// TODO: This is a hack, should be done otherwise, but how
#ifdef _WIN32
	string nullbuf = "NUL";
#else
	string nullbuf = "/dev/null";
#endif
	// Only rank 0 processor will ever output anything!
	//if (g_mpi_data.rank != 0) {
	//	g_debug_stream.open(nullbuf);
	//	g_orbit_stream.open(nullbuf);
	//	g_cut_stream.open(nullbuf);
	//	g_disk_stream.open(nullbuf);
	//	g_accretion_stream.open(nullbuf);
	//	g_particle_stream.open(nullbuf);
	//	g_stat_stream.open(nullbuf);
	//	g_element_stream.open(nullbuf);
	//	g_diag_stream.open(nullbuf);
	//}
	g_debug_stream.open(	g_progconf.debug_file.c_str());
	g_debug_stream.setf(std::ios::unitbuf); // Disable buffering for debug stream
	g_orbit_stream.open(	g_progconf.orbit_file.c_str());
	g_cut_stream.open(		g_progconf.cut_file.c_str());
	g_disk_stream.open(		g_progconf.disk_file.c_str());
	g_accretion_stream.open(g_progconf.accretion_file.c_str());
	g_particle_stream.open(	g_progconf.particle_file.c_str());
	g_stat_stream.open(		g_progconf.stat_file.c_str());
	g_element_stream.open(	g_progconf.element_file.c_str());
	g_diag_stream.open(		g_progconf.diag_file.c_str());
}

/// Close all output streams
void close_streams() {
	g_debug_stream.close();
	g_orbit_stream.close();
	g_cut_stream.close();
	g_disk_stream.close();
	g_accretion_stream.close();
	g_particle_stream.close();
	g_stat_stream.close();
	g_element_stream.close();
	g_diag_stream.close();
}

// Explicitly flush all streams, including stdio and stderr
void flush_streams() {
	g_debug_stream.flush();
	g_orbit_stream.flush();
	g_cut_stream.flush();
	g_disk_stream.flush();
	g_accretion_stream.flush();
	g_particle_stream.flush();
	g_stat_stream.flush();
	g_element_stream.flush();
	g_diag_stream.flush();
	std::cout.flush();
}

#define DEBUG_PRINT 0
void debug_print(FILE *stream, const char *preamble, const int &v) {
#if DEBUG_PRINT
	fprintf(stream, "%15s: %16d\n", preamble, v);
#endif
}
void debug_print(FILE *stream, const char *preamble, const double &v) {
#if DEBUG_PRINT
	fprintf(stream, "%15s: %16.10e\n", preamble, v);
#endif
}
void debug_print(FILE *stream, const char *preamble, const Vector3d &v) {
#if DEBUG_PRINT
	fprintf(stream, "%15s: %16.10e %16.10e %16.10e\n", preamble, v[0], v[1], v[2]);
#endif
}

// DEBUG: function to check and print single particle status
#if 0
void db(State &s, int line, char* file) {
	int n=911;
	g_debug_stream << "p[n]: " << s.objs.parts[n].state
		<< " " << s.objs.parts[n].r.transpose()
		<< " " << s.objs.parts[n].v.transpose() << std::endl;
	for (int i=0; i < 3; i++) {
		if (!_finite(s.objs.parts[n].r[i]) ||
			!_finite(s.objs.parts[n].v[n])) {
			std::cerr << "NAN in comp " << i << " file " << file << " line " << line << std::endl;
			return;
		}
	}
}
#endif

// Output format with double precision.
Eigen::IOFormat eigen_format(15);
