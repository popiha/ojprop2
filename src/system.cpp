#include <Eigen/Dense>
using namespace Eigen;

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/math/special_functions/atanh.hpp>

#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cstdio>
#include <string>
#include <limits>

#include "conf.hpp"
#include "system.hpp"
#include "units.hpp"
#include "pn.hpp"
#include "grid.hpp"
#include "disk.hpp"
#include "misc.hpp"
#include "globals.hpp"
#include "propagator.hpp"


const unsigned int DEFAULT_SEED = 1337U;

// Helper function for z-sorting, when lambdas cannot be used
// (GCC <= 4.4)
#ifdef __GNUC__
static bool z_sort(const Vector3d &a, const Vector3d &b) { return a.z() < b.z(); };
#endif

// Input/output facilities of bhole and particle
void bhole::serialize_to_stream(ostream &os) const {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(15);
	ss << id << " ";
	ss 	<< std::setprecision(15) << std::scientific;
	ss << m << " ";
	ss << acc_r << " ";
	ss  << r.transpose().format(eigen_format) << " "	
		<< v.transpose().format(eigen_format) << " "
		<< s.transpose().format(eigen_format) << " ";
	ss << chi << " ";
	os << ss.str();
}

void bhole::serialize_from_stream(istream &is) {
	is >> id;
	is >> m;
	is >> acc_r; 
	for (int j=0; j < 3; j++) is >> r[j];
	for (int j=0; j < 3; j++) is >> v[j];
	for (int j=0; j < 3; j++) is >> s[j];
	is >> chi;
	// Precalculate accretion radii
	//acc_r = 2 * C_G * m / (C_c*C_c);
}

void particle::serialize_to_stream(ostream &os) const {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(15);
	ss << id << " ";
	ss << state << " ";
	ss 	<< std::setprecision(15) << std::scientific;
	if (is_massless)
		ss << 0.0 << " ";
	else
		ss << m << " ";
	ss  << r.transpose().format(eigen_format) << " "	
		<< v.transpose().format(eigen_format) << " ";
	os << ss.str();
}

void particle::serialize_from_stream(istream &is) {
	is >> id;
	{
		int state_int;

		if (!(is >> state_int)) { /* handle error */ }
		state = static_cast<ParticleState>(state_int);
	}
	is >> m;
	for (int j=0; j < 3; j++) is >> r[j];
	for (int j=0; j < 3; j++) is >> v[j];
	if (fabs(m) <= numeric_limits<double>::min()) 
		is_massless = true;
}

// Read an initial value from an ifstream and dump it's value to debugstream
template <typename T> static void read_value(ifstream &f, T &value) {
	f >> value;
	if (!f.good()) {
		cerr << "Error reading initial values." << endl;
		exit(EXIT_FAILURE);
	}
	if (g_progconf.debug_enabled("initial_values"))
		g_debug_stream << "Read an initial value: " << value << endl;
}

// Load initial conditions from a file
void State::load_from_file(char *filename) {
	ifstream f(filename);
	if (f.good()) {
		g_debug_stream << "Loading initial conditions from " << filename << "\n";
	}
	else {
		cerr << "Error opening file " << filename << "\n";
		exit(EXIT_FAILURE);
	}

	// Physical configuration comes first
	{
		// TODO: We should be able to set the initial _physical_ time
		// in the initial conditions file.
		read_value(f, physical.t);
		physical.t = 0;

		read_value(f, physical.z);
		read_value(f, physical.q);
	}
	// Then comes black hole and particle data
	{
		ULONG n_bhs, n_parts;
		read_value(f, n_bhs);
		read_value(f, n_parts);

		objs.bhs.reserve(n_bhs);
		objs.parts.reserve(n_bhs);

		// Calculate the position and velocity of the center of mass as we go along
		Vector3d r_cm = Vector3d::Zero(), v_cm = Vector3d::Zero();

		// A black hole. Input values, dimension in ():
		// id(1), mass(1), position(3), velocity(3), spin direction(3),
		// spin magnitude(1)
		objs.m_tot_bhs = 0;
		for (ULONG i=0; i < n_bhs; i++) {
			bhole b;

			b.serialize_from_stream(f);
			objs.bhs.push_back(b);
			objs.m_tot_bhs += b.m;
			r_cm += b.m*b.r;
			v_cm += b.m*b.v;
		}
		// A massive or non-massive particle. Input values, dimension in ():
		// id(1), mass(1), position(3), velocity(3)
		// These must be organized so that the massive particles come first.
		objs.m_tot_parts = 0;
		objs.first_massless_part = 0;
		for (ULONG i=0; i < n_parts; i++) {
			particle p;

			p.serialize_from_stream(f);
			if (p.is_massless)
				if (objs.first_massless_part == 0)
					objs.first_massless_part = i;
			objs.parts.push_back(p);
			objs.m_tot_parts += p.m;
			r_cm += p.m*p.r;
			v_cm += p.m*p.v;
		}

		// Now, get center of mass position and velocity, and make sure
		// everything is in cm-coordinates
		r_cm /= objs.m_tot_bhs + objs.m_tot_parts;
		v_cm /= objs.m_tot_bhs + objs.m_tot_parts;

		for (ULONG i=0; i < objs.bhs.size(); i++) {
			objs.bhs[i].r -= r_cm;
			objs.bhs[i].v -= v_cm;
		}
		for (ULONG i=0; i < objs.parts.size(); i++) {
			objs.parts[i].r -= r_cm;
			objs.parts[i].v -= v_cm;
		}
	}
	
	// Update physical constants
	update_constants(constants);
	update_constants(initial_constants);

	// Initialize stat accumulators
	// TODO: This might be somewhere else. Now the stat system isn't quite independent
	init_accumulators();
}

std::string State::bholes_to_string() const {
	std::stringstream ss;
	ss 	<< std::fixed << std::setprecision(15) << physical_time() << " ";
		// Constants of motion
	ss  << std::scientific
		<< (constants.E_kep - initial_constants.E_kep - constants.E_gr)/initial_constants.E_kep << " "
		<< constants.E_gr/initial_constants.E_kep << " "
		<< constants.h.transpose().format(eigen_format) << " ";
	for (ULONG i=0; i < objs.bhs.size(); i++) 
		ss  << objs.bhs[i].r.transpose().format(eigen_format) << " "	
			<< objs.bhs[i].v.transpose().format(eigen_format) << " "
			<< objs.bhs[i].s.transpose().format(eigen_format) << " ";
	return ss.str();
}

std::string State::parts_to_string() const {
	std::stringstream ss;
	for (ULONG i=0; i < objs.parts.size(); i++) {
		const particle &p = objs.parts[i];
		// TODO: Here we leave out particles that have been escaped or accreted.
		// It is concievable one might want them printed out
		if (p.state != NORMAL) continue;
#if 0
		ss << std::fixed << std::setprecision(15) << physical_time() << " " << std::scientific;
		ss	<< p.id << " " << p.state << " "
			<< p.r.transpose().format(eigen_format) << " "
			<< p.v.transpose().format(eigen_format) << "\n";
#endif
		ss << std::fixed << std::setprecision(15) << physical_time() << " " << std::scientific;
		p.serialize_to_stream(ss);
		ss << "\n";
	}
	return ss.str();
}

std::string State::stat_accumulators_to_string() const {
	std::stringstream ss;
	ss 	<< std::fixed << std::setprecision(15) 
		<< physical_time();
	ss << " " << stats.inclination_escape_count;
	for (int i=0; i < stats.accretion_count.size(); i++)
		ss << " " << stats.accretion_count[i];
	return ss.str();
}

std::string State::diagnostics_to_string() const {
	std::stringstream ss;
	ss 	<< std::scientific << std::setprecision(15)

		// Black hole contribution
		// Keplerian + GR energy
		<< (constants.E_kep - initial_constants.E_kep - constants.E_gr)/initial_constants.E_kep << " "

		// GR energy
		<< constants.E_gr/initial_constants.E_kep << " "

		// Total angular momentum
		<< constants.h.transpose().format(eigen_format) << " ";

		// Particle contribution
		// Keplerian + GR energy
	// TODO: Calculate particle diagnostics
	return ss.str();
}

double State::physical_time() const {
	// TODO: Valtonen/Mikkola has a strange factor 1/0.2054 as a
	// scaling of time. Where does it come from?
	if (g_progconf.tweak_enabled("time"))
		return physical.t*0.2054 + g_progconf.start_year;

	return (1+physical.z)*physical.t + g_progconf.start_year;
}


void State::update_constants(consts &c) {
	c.E_kep = 0;
	for (ULONG i=0; i < objs.bhs.size(); i++) {
		bhole & pi = objs.bhs[i];
		c.E_kep += 0.5 * pi.m/objs.m_tot_bhs * pi.v.squaredNorm();
		for (int j=i+1; j < objs.bhs.size(); j++) {
			bhole & pj = objs.bhs[j];
			c.E_kep += -C_G * pi.m * pj.m / (objs.m_tot_bhs * (pi.r - pj.r).norm());
		}
	}
	c.h.setZero();
	for (ULONG i=0; i < objs.bhs.size(); i++) {
		bhole & pi = objs.bhs[i];
		c.h += pi.m/objs.m_tot_bhs * pi.r.cross(pi.v);
	}

	// TODO: Particles (massive or not) not included in constant of motion calculations!
#if 0
	for (int i=0; i < objs.first_massless_part; i++) {
		particle & pi = objs.parts[i];
		constants.E_kep += 0.5 * pi.m/objs.m_tot_parts * pi.v.squaredNorm();
		for (int j=i+1; j < objs.parts.size(); j++) {
			particle & pj = objs.parts[j];
			constants.E_kep += -C_G * pi.m * pj.m / (objs.m_tot_parts * (pi.r - pj.r).norm());
		}
	}
#endif
	//bin.h = m_rel1*m_rel2*bin.r.cross(bin.v);
}


void State::update_element_stats() {
#if 0
	// Orbital elements
	const double mu = C_G * m.m1;
	double oe[6];
	double acc[6][2] = {{0}};
	double min[6] = {0};
	double max[6] = {0};
	int num=0;

	for (int i=0; i < 6; i++) {
		min[i] = std::numeric_limits<double>::max();
		max[i] = std::numeric_limits<double>::min();
	}

	// Mean, min, max
#pragma omp parallel for private (oe) if (g_progconf.use_openmp)
	for (int i=0; i < disk1.n; i++) {
		if (disk1.states[i] != disk::NORMAL)
			continue;

#pragma omp atomic
		num++;

		// Calculate orbital elements
		Vector3d pos = disk1.rs[i] - bin.rcm1;
		Vector3d vel = disk1.vs[i] - bin.vcm1;
		rv2oe_gp(mu, pos, vel, oe);

		// Handle angles separately. Order is:
		// a (scalar)
		// e (scalar)
		// i (angle)
		// Omega (angle)
		// omega (angle)
		// M (angle)
#pragma omp atomic
		acc[0][0] += oe[0];
#pragma omp atomic
		acc[1][0] += oe[1];
#pragma omp atomic
		acc[2][0] += std::cos(oe[2]);
#pragma omp atomic
		acc[2][1] += std::sin(oe[2]);
#pragma omp atomic
		acc[3][0] += std::cos(oe[3]);
#pragma omp atomic
		acc[3][1] += std::sin(oe[3]);
#pragma omp atomic
		acc[4][0] += std::cos(oe[4]);
#pragma omp atomic
		acc[4][1] += std::sin(oe[4]);
#pragma omp atomic
		acc[5][0] += std::cos(oe[5]);
#pragma omp atomic
		acc[5][1] += std::sin(oe[5]);

#pragma omp critical
		{
			for (int j=0; j < 6; ++j) {
				{
					min[j] = std::min(oe[j], min[j]);
					max[j] = std::max(oe[j], max[j]);
				}
			}
		}
	}

	for (int j=0; j <= 1; ++j) {
		// Store mean in acc[j][0], disregard acc[j][1]
		// (which doesn't even have any meaningful data)
		acc[j][0] /= num;
		stats.orbital_elements[j][1] = min[j];
		stats.orbital_elements[j][2] = max[j];
	}

	for (int j=2; j <= 5; ++j) {
		acc[j][0] /= num;
		acc[j][1] /= num;
		stats.orbital_elements[j][1] = min[j];
		stats.orbital_elements[j][2] = max[j];

		// We can compute standard deviation for angles right away:
		// S(z) = sqrt(-2 * log |mean(z)|) = sqrt(log(1/|mean(z)|^2))
		double z2 = acc[j][0]*acc[j][0] + acc[j][1]*acc[j][1];
		stats.orbital_elements[j][3] = sqrt(log(1/z2));
	}

	// Store means
	stats.orbital_elements[0][0] = acc[0][0];
	stats.orbital_elements[1][0] = acc[1][0];
	stats.orbital_elements[2][0] = std::atan2(acc[2][1], acc[2][0]);
	stats.orbital_elements[3][0] = std::atan2(acc[3][1], acc[3][0]);
	stats.orbital_elements[4][0] = std::atan2(acc[4][1], acc[4][0]);
	stats.orbital_elements[5][0] = std::atan2(acc[5][1], acc[5][0]);

	// Calculate standard deviations for the first two orbital elements
	acc[0][0] = acc[1][0] = 0;
#pragma omp parallel for private(oe) if (g_progconf.use_openmp)
	for (int i=0; i < disk1.n; i++) {
		if (disk1.states[i] != disk::NORMAL)
			continue;
		// Calculate orbital elements
		Vector3d pos = disk1.rs[i] - bin.rcm1;
		Vector3d vel = disk1.vs[i] - bin.vcm1;
		rv2oe_gp(mu, pos, vel, oe);

		// Handle angles separately. Order is:
		// a (scalar)
		// e (scalar)
		// i (angle)
		// Omega (angle)
		// omega (angle)
		// M (angle)
		double d1 = oe[0] - stats.orbital_elements[0][0];
		double d2 = oe[1] - stats.orbital_elements[1][0];
#pragma omp atomic
		acc[0][0] += d1*d1;
#pragma omp atomic
		acc[1][0] += d2*d2;
	}
	stats.orbital_elements[0][3] = sqrt(acc[0][0]/(num-1));
	stats.orbital_elements[1][3] = sqrt(acc[1][0]/(num-1));
#endif
}

std::string State::disk_oe_stats_to_string() const {
	std::stringstream ss;
	ss 	<< std::setprecision(15) << std::scientific;
	ss << physical_time();
	for (int i=0; i < 6; i++) {
		for (int j=0; j < 4; j++)
			ss << " " << stats.orbital_elements[i][j];
	}
	return ss.str();
}
