#include "propagator.hpp"
#include "system.hpp"
#include "pn.hpp"
#include "conf.hpp"
#include "units.hpp"
#include "globals.hpp"
#include "viscosity.hpp"
#include "misc.hpp"

#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <algorithm>

#include <cstdio>

// BS-integrator
extern void bsstep(State &s, double *xx, double htry, double eps, double *hdid, double *hnext);

void check_accretion(State &s);
void check_escapes(State &s);

#if 0
double omega_binary(const State &s) {
	return 1/s.bin.r.norm();
}
#endif

double omega(const State &s) {
	double om = 0;
#pragma omp parallel for reduction(+:om) if (g_progconf.use_openmp)
	for (long i=0; i < s.objs.bhs.size(); i++) {
		for (long j=i+1; j < s.objs.bhs.size(); j++)
			om = om + 1/(s.objs.bhs[j].r-s.objs.bhs[i].r).norm();
	}

	return om;
}

void check_escapes(State &s) {
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < s.objs.parts.size(); i++) {
		particle &p = s.objs.parts[i];
		if (p.state != NORMAL) continue;
		if (p.r.norm() > g_progconf.escape_radius) {
#pragma omp critical
			{
				p.state = ESCAPED;
				g_particle_stream 
					<< s.physical_time()  << " " << p.id << " "
					<< "ESCAPED" << " r = " << p.r.norm() << "\n";
			}
		}
	}
}

void check_accretion(State &s) {
	// Return if accretion not enabled
	if (!g_progconf.disk_accretion)
		return;

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long j=0; j < s.objs.parts.size(); j++) {
		particle &pj = s.objs.parts[j];
		if (pj.state != NORMAL) continue;

		for (long i=0; i < s.objs.bhs.size(); i++) {
			bhole &pi = s.objs.bhs[i];
			
			double racc2 = pi.acc_r * pi.acc_r;
			double dist = (pj.r - pi.r).squaredNorm();
			if (dist <= racc2) {
#pragma omp critical 
				{
					// Accreted to black hole i
					pj.state = ACCRETED;
					s.stats.accrete_particle(i);
					g_particle_stream << s.physical_time()  << " " << pj.id << " ACCRETED to " << pi.id << endl;
				}
			}
		}
	}
}


void propagate(State &s, double timestep) {
	// Propagate with Bulirsch-Stoer
	static double t = 0, hdid=0, hnext=timestep;

	// Recalculate TTL w
	if (g_progconf.use_TTL) {
		double new_w = omega(s);
		s.integrator_state.ttlw = new_w;
	}

	// Mikkola's esoteric timestep selection?
	// Mikkola has velocities divided by sqrt(W) in timestep selection, so do same
#if 0
	double ordv = ((1/sqrt(s.ttlw))*s.bin.v).array().abs().sum();
	debug_print(stderr, "ordv", ordv);
	debug_print(stderr, "sm", s.pc_bin.m);
	debug_print(stderr, "W", s.ttlw);
	debug_print(stderr, "Mikkola: .3/(ordv+sqrt(sm*W)/NB)", 0.3/(ordv + sqrt(s.pc_bin.m*s.ttlw)/2));

	hnext = std::min(hnext, 0.3/(ordv + sqrt(s.pc_bin.m*s.ttlw)/2));
	debug_print(stderr, "Mikkola selection: new hnext", hnext);
#endif

	// Cap timestep
	// TODO: Facilitate negative timesteps?
	if (hnext > g_progconf.max_timestep) {
		if (g_progconf.debug_enabled("timestep"))
			g_debug_stream << "Timestep: wanted " << hnext << ", capped at max " << g_progconf.max_timestep << endl;
		hnext = g_progconf.max_timestep;
	}
	if (hnext < g_progconf.min_timestep) {
		if (g_progconf.debug_enabled("timestep"))
			g_debug_stream << "Timestep: wanted " << hnext << ", capped at min " << g_progconf.min_timestep << endl;
		hnext = g_progconf.min_timestep;
	}
	/*
	hnext = std::min(hnext, g_progconf.max_timestep);
	hnext = std::max(hnext, g_progconf.min_timestep);
	*/

	//fprintf(stderr, "stepping with %16.10e\n", hnext);

	// Make one step
	bsstep(s, &t, hnext, g_progconf.bs_epsilon, &hdid, &hnext);		

	if (g_progconf.debug_enabled("timestep"))
		g_debug_stream << "Timestep: did " << hdid << " next " << hnext << endl;

	// Now update every dependent value that is needed for any
	// purpose before the beginning of the next step, or in the
	// next step

	// Recalculate keplerian energy.
	s.update_constants(s.constants);

	// Check for escapes
	check_escapes(s);

	// Check for accretion
	// TODO: Also done now in rleap in bs.cpp, shouldn't be necessary here
	check_accretion(s);
}

// Check accretion with positions linearly interpolated
static void do_accretion(State &s, double dt) {
	// Return if accretion not enabled
	if (!g_progconf.disk_accretion)
		return;

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long j=0; j < s.objs.parts.size(); j++) {
		particle &pj = s.objs.parts[j];
		if (pj.state != NORMAL) continue;

		for (long i=0; i < s.objs.bhs.size(); i++) {
			bhole &pi = s.objs.bhs[i];
			
			double racc2 = pi.acc_r * pi.acc_r;
			Vector3d rij = pj.r - pi.r;
			Vector3d vij = pj.v - pi.v;
			double rv = rij.dot(vij);

			// When will there be a collision? Use a linear approximation.
			double t = (racc2 - rij.squaredNorm())/(2 * rv);

			// Accretion event happens only if the particles ventures within the accretion radius
			// during the step
			if (0 <= t && t <= dt) {
#pragma omp critical 
				{
					// Accreted to black hole i
					pj.state = ACCRETED;
					s.stats.accrete_particle(i);
					// TODO: Move this away from here, causes one event to be multiply reported
					g_particle_stream << s.physical_time()  << " " << pj.id << " ACCRETED to " << pi.id << endl;
				}
			}
		}
	}
}

void rleap(State &s, const double h) {
	const double hs = (g_progconf.use_TTL ? h/s.integrator_state.ttlw : h);
	// We must check for accretion here.
	do_accretion(s, hs);

	s.physical.t += hs;
	// TODO: Are these necessary to avoid using size() in a for loop? And does it even matter?
	// And if so, move them under objs
	long bhn = s.objs.bhs.size();
	long partn = s.objs.parts.size();

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < bhn; i++)
		s.objs.bhs[i].r += hs * s.objs.bhs[i].v;

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < partn; i++) {
		if (s.objs.parts[i].state != NORMAL) continue;
		s.objs.parts[i].r += hs * s.objs.parts[i].v;
	}
}

template <typename A, typename B> static Vector3d rhat_over_r2(A &pi, B &pj) {
		Vector3d rij = pj.r - pi.r;
		double r = rij.norm();
		Vector3d f = (1/(r*r*r))*rij;
		return f;
}

static bool check_pn_divergence(const string id, int it, double old_value, double new_value) {
	if (new_value > old_value) {
		g_debug_stream << "PN-iteration: Diverged at iteration " << it << " for " << id 
				<< ". Old: " << old_value << " New: " << new_value << endl;
		return true;
	}
	return false;
}


static void kepler_acceleration(const State &s, VectorXd &kep_acc_bhs, VectorXd &kep_acc_parts, VectorXd &omega_derivs) {
	const long bhn = s.objs.bhs.size();
	const long partn = s.objs.parts.size();

	// Black holes on black holes
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < bhn; i++) {
		const bhole &pi = s.objs.bhs[i];
		for (long j = 0; j < bhn; j++) {
			if (i == j) continue;
			const bhole &pj = s.objs.bhs[j];

			const Vector3d f = rhat_over_r2(pi, pj);
			kep_acc_bhs.segment<3>(3*i) += f*pj.m*C_G;
			// Use only black holes for TTL
			omega_derivs.segment<3>(3*i) += f;
		}
	}

	// Black holes on particles
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < partn; i++) {
		const particle &pi = s.objs.parts[i];
		if (pi.state != NORMAL) continue;

		for (long j = 0; j < bhn; j++) {
			const bhole &pj = s.objs.bhs[j];

			const Vector3d f = rhat_over_r2(pi, pj);
			kep_acc_parts.segment<3>(3*i) += f*pj.m*C_G;
		}
	}

	// If we have massive particles, do particle -> bhole, particle -> particle
	if (s.objs.first_massless_part > 0) {

	// Particles on black holes, inner loop only over massive particles
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < bhn; i++) {
			const bhole &pi = s.objs.bhs[i];
			for (long j = 0; j < s.objs.first_massless_part; j++) {
				const particle &pj = s.objs.parts[j];
				if (pj.state != NORMAL) continue;
	
				const Vector3d f = rhat_over_r2(pi, pj);
				kep_acc_bhs.segment<3>(3*i) += f*pj.m*C_G;
			}
		}
	
		// Particles on particles, inner loop only over massive particles
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < partn; i++) {
			const particle &pi = s.objs.parts[i];
			if (pi.state != NORMAL) continue;
	
			for (long j = 0; j < s.objs.first_massless_part; j++) {
				if (i == j) continue;
				const particle &pj = s.objs.parts[j];
				if (pj.state != NORMAL) continue;

				const Vector3d f = rhat_over_r2(pi, pj);
				kep_acc_parts.segment<3>(3*i) += f*pj.m*C_G;
			}
		}
	}
}

#if 1
void vleap(State &s, const double h) {
	double omg = (g_progconf.use_TTL ? omega(s) : 1);
	double hs = h/omg;

	// TODO: Are these necessary to avoid using size() in a for loop?
	// And if so, move them under objs
	long bhn = s.objs.bhs.size();
	long partn = s.objs.parts.size();

	// TODO:
	// Pit�isk� bhole/particlen olla jokin luokka, joka osaa ilmoittaa montako 
	// dynaamista muuttujaa sill� on, ja antaa ne tarvittessa flat array -muodossa?
	// Ja mahdollisesti lis�ksi toimia komponentti-tyyliin (accretion-komponentti,
	// PN-correction inducing -komponentti jne)?

	// 
	// Keplerian accelerations for bholes and particles
	VectorXd kep_acc_bhs = VectorXd::Zero(3 * bhn);
	VectorXd kep_acc_parts = VectorXd::Zero(3 * partn+1);
	// Also store all terms of form (r_j - r_i)/|r_j - r_i|^3 needed for TTL
	VectorXd omega_derivs = VectorXd::Zero(3 * bhn);

	kepler_acceleration(s, kep_acc_bhs, kep_acc_parts, omega_derivs);
	// 
	// Add disk internal interactions to kepler force, to average
	// them over the step
	// TODO: Internal interactions removed for now

	// TODO: refaktoroi PN-iteraatio:
	// - pit�� olla virhetilanne, jos iteraatio ei konvergoidu (ainakin mustille aukoille)! 
	//		=> pienennet��n aika-askelta, tai laitetaan nollaksi tai jotain

	// We will calculate average velocity v_avg over step size h, starting with 
	// an initial guess v_avg0 = v0 + h/2 * kep_accel, and using this to
	// calculate PN-acceleration pn_accel0. 
	// Then we find v_avg1 = v0 + h/2 * (kep_accel + pn_accel0), and use
	// this to calculate pn_accel1 and so on, until pn_accel converges.
	//
	// The check for convergence is comparison of 
	// sum_i |v_avg(n+1)_i - v_avgn_i| / sum_i | v_avg0_i| 
	// i.e. comparing the sums of absolute values of differences in components of v_avg.
	//
	// Spins are updated similarly to velocities, except that there is obviously
	// no Keplerian contribution.

	// Initial average velocities for bholes and particles and initial spins for bholes
	VectorXd avg_vel_bhs = hs/2 * kep_acc_bhs;
	VectorXd avg_vel_parts = hs/2 * kep_acc_parts;
	VectorXd avg_spin_bhs(3 * bhn);
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < bhn; i++) {
		avg_vel_bhs.segment<3>(3*i) += s.objs.bhs[i].v;
		avg_spin_bhs.segment<3>(3*i) = s.objs.bhs[i].s;
	}
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < partn; i++) {
		if (s.objs.parts[i].state != NORMAL) continue;
		avg_vel_parts.segment<3>(3*i) += s.objs.parts[i].v;
	}

	// Initial sums of absolute values
	double avg_vel_bhs_avsum = avg_vel_bhs.array().abs().sum();
	double avg_spin_bhs_avsum = avg_spin_bhs.array().abs().sum();
	double avg_vel_parts_avsum = avg_vel_parts.array().abs().sum();

	// PN-accelerations & spin derivatives
	VectorXd pn_acc_bhs = VectorXd::Zero(3 * bhn);
	VectorXd pn_dspin_bhs = VectorXd::Zero(3 * bhn);
	VectorXd pn_acc_parts = VectorXd::Zero(3 * partn+1);

	// Sums of absolute values of differences
	double diff_vel_bhs = 0;
	double diff_spin_bhs = 0;
	double diff_vel_parts = 0;

	// Start iteration
	int iteration = 0;
	while (true) {
		++iteration;
		// PN-corrections are calculated pairwise, and must be done in relative coordinates
		// TODO: There is room for optimization in speed with memory cost here: r's stay constant,
		// so we could calculate them once, store, and use again
		
		pn_acc_bhs.setZero();
		pn_dspin_bhs.setZero();
		pn_acc_parts.setZero();
		
		// Black holes on black holes
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i=0; i < bhn; i++) {
			bhole &pi = s.objs.bhs[i];

			for (long j=i+1; j < bhn; j++) {
				
				bhole &pj = s.objs.bhs[j];
				double total_mass = pi.m + pj.m;
				Vector3d rij = pj.r - pi.r;
				Vector3d vij = avg_vel_bhs.segment<3>(3*j) - avg_vel_bhs.segment<3>(3*i);
				Vector3d si = avg_spin_bhs.segment<3>(3*i);

				Vector3d pn_acc = pn_terms(C_G, C_c, pi.m, pj.m, rij, vij) 
						+ pn_spin_terms(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				// TODO: Must have spin-spin terms here, now using only chi and spin of pi!
				// TODO: Need to use both components from av_spin_bhs
				Vector3d pn_dspin = pn_sdot(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				//cerr << "pnacc " << pn_acc.transpose().format(eigen_format) << endl;
				//cerr << "pndsp " << pn_dspin.transpose().format(eigen_format) << endl;

				pn_acc_bhs.segment<3>(3*i) -= pn_acc*pj.m/total_mass;
				pn_dspin_bhs.segment<3>(3*i) += pn_dspin;
#pragma omp critical
				{
				pn_acc_bhs.segment<3>(3*j) += pn_acc*pi.m/total_mass;
				}
			}
		}

		// Black holes on particles.
		// NOTE: As an optimization, particles DO NOT cause PN-corrections on black holes
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long j=0; j < partn; j++) {
			particle &pj = s.objs.parts[j];
			if (pj.state != NORMAL) continue;
			for (long i=0; i < bhn; i++) {
				bhole &pi = s.objs.bhs[i];
				double total_mass = pi.m + pj.m;
				Vector3d rij = pj.r - pi.r;
				Vector3d vij = avg_vel_parts.segment<3>(3*j) - avg_vel_bhs.segment<3>(3*i);
				Vector3d si = avg_spin_bhs.segment<3>(3*i);

				Vector3d pn_acc = pn_terms(C_G, C_c, pi.m, pj.m, rij, vij) 
					+ pn_spin_terms(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				// TODO: Must have spin-spin terms here, now using only chi and spin of pi!
				Vector3d pn_dspin = pn_sdot(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 

				pn_acc_parts.segment<3>(3*j) += pn_acc*pi.m/total_mass;
				// NOTE: No PN-correction by particles on black holes
#if 0
#pragma omp critical 
				{
					pn_acc_bhs.segment<3>(3*i) -= pn_acc*pj.m/total_mass;
					pn_dspin_bhs.segment<3>(3*i) += pn_dspin;
				}
#endif
			}
		}
		// NOTE: No PN-corrections between particles
		//cerr << "pnacc " << pn_acc_bhs.transpose().format(eigen_format) << endl;
		//cerr << "pndsp " << pn_dspin_bhs.transpose().format(eigen_format) << endl;
		//exit(0);

		// Update average velocities, and store the old values
		VectorXd avg_vel_bhs_old = avg_vel_bhs;
		VectorXd avg_spin_bhs_old = avg_spin_bhs;
		VectorXd avg_vel_parts_old = avg_vel_parts;

		avg_vel_bhs = hs/2*(kep_acc_bhs + pn_acc_bhs);
		avg_spin_bhs = hs/2*pn_dspin_bhs;
		avg_vel_parts = hs/2*(kep_acc_parts + pn_acc_parts);

#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < bhn; i++) {
			avg_vel_bhs.segment<3>(3*i) += s.objs.bhs[i].v;
			avg_spin_bhs.segment<3>(3*i) += s.objs.bhs[i].s;
		}
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < partn; i++) {
			if (s.objs.parts[i].state != NORMAL) continue;
			avg_vel_parts.segment<3>(3*i) += s.objs.parts[i].v;
		}

		// Now check for convergence
		double eps = g_progconf.pn_epsilon;
		double diff_vel_bhs_old = diff_vel_bhs;
		double diff_spin_bhs_old = diff_spin_bhs;
		double diff_vel_parts_old = diff_vel_parts;
		diff_vel_bhs = (avg_vel_bhs - avg_vel_bhs_old).array().abs().sum();
		diff_spin_bhs = (avg_spin_bhs - avg_spin_bhs_old).array().abs().sum();
		diff_vel_parts = (avg_vel_parts - avg_vel_parts_old).array().abs().sum();

		//cerr << "kepf " << (kep_acc_bhs.segment<3>(3)-kep_acc_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "dv   " << (pn_acc_bhs.segment<3>(3)-pn_acc_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "va   " << (avg_vel_bhs.segment<3>(3)-avg_vel_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "dsp  " << pn_dspin_bhs.transpose() << endl;

		// First, divergence check
		if (iteration > 1 && (check_pn_divergence("BH velocity", iteration, diff_vel_bhs_old, diff_vel_bhs)
			|| check_pn_divergence("BH spin", iteration, diff_spin_bhs_old, diff_spin_bhs)
			|| check_pn_divergence("Particle velocity", iteration, diff_vel_parts_old, diff_vel_parts))) {
			// We have divergence. We should lower timestep!
			// TODO: Abort and lower timestep
			break;
		}

		// Check convergence conditions
		bool conv_vel_bhs = diff_vel_bhs <= eps * avg_vel_bhs_avsum;
		bool conv_spin_bhs = diff_spin_bhs <= eps * avg_spin_bhs_avsum;
		bool conv_vel_parts = diff_vel_parts <= eps * avg_vel_parts_avsum;

		if (conv_vel_bhs && conv_spin_bhs && conv_vel_parts) {
			// Convergence
			break;
		}
		else if (iteration >= g_progconf.pn_max_it) {
			// Ran out of iterations. Print the parts that failed to converge
			if (!conv_vel_bhs)
				g_debug_stream << "PN iteration: BH velocity not converged in " 
					<< iteration << " iterations. Residual: " << diff_vel_bhs 
					<< " Tolerance: " << eps * avg_vel_bhs_avsum << endl;
			if (!conv_spin_bhs)
				g_debug_stream << "PN iteration: BH spin not converged in " 
					<< iteration << " iterations. Residual: " << diff_spin_bhs 
					<< " Tolerance: " << eps * avg_spin_bhs_avsum << endl;
			if (!conv_vel_parts)
				g_debug_stream << "PN iteration: Particle velocity not converged in " 
					<< iteration << " iterations. Residual: " << diff_vel_parts 
					<< " Tolerance: " << eps * avg_vel_parts_avsum << endl;

			// TODO: Should abort substeps, lower timestep and try again?
			break;
		}
	}

	// Make extra sure that there are no infinities or nans around
	// TODO: Do these work?
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < pn_acc_bhs.size(); i++) {
		if (!custom_isfinite(pn_acc_bhs[i])) {
#pragma omp critical
			{
				g_debug_stream << "PN iteration: pn_acc_bhs["<<i<<"] NOT FINITE!" << endl;
				pn_acc_bhs[i] = 0.0;
			}
		}
	}

	// Now, update velocities & spins to reflect state at end of step
	kep_acc_bhs += pn_acc_bhs;
	kep_acc_parts += pn_acc_parts;

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < bhn; i++) {
		s.objs.bhs[i].v += hs * kep_acc_bhs.segment<3>(3*i);
		s.objs.bhs[i].s += hs * pn_dspin_bhs.segment<3>(3*i);
	}
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < partn; i++) {
		if (s.objs.parts[i].state != NORMAL) continue;
		s.objs.parts[i].v += hs * kep_acc_parts.segment<3>(3*i);
	}

	// Calculate change in GR energy
	double egdot = 0;
	for (long i=0; i < bhn; i++) {
		egdot += s.objs.bhs[i].m / s.objs.m_tot_bhs * 
			avg_vel_bhs.segment<3>(3*i).dot(pn_acc_bhs.segment<3>(3*i));
	}
	s.constants.E_gr += hs * egdot;

	// Calculate change in TTL W-value
	double wdot = 0;
	wdot = avg_vel_bhs.dot(omega_derivs);

	s.integrator_state.ttlw += hs * wdot;

	//cerr << "rij " << (s.objs.bhs[1].r - s.objs.bhs[0].r).transpose() << endl;
	//cerr << "vij " << (avg_vel_bhs.segment<3>(3)-avg_vel_bhs.segment<3>(0)).transpose() << endl;
	//cerr << "oders " << omega_derivs.transpose() << endl;
	//cerr << "wdot " << wdot << " egdot " << egdot << endl;
	//cerr << "hs " << hs << endl;
	//exit(0);
}
#else
void vleap(State &s, double h) {
	const double pn_eps = g_progconf.pn_epsilon;
	double omg = (g_progconf.use_TTL ? omega(s) : 1);
	double hs = h/omg;

	// TODO: Are these necessary to avoid using size() in a for loop?
	// And if so, move them under objs
	auto bhn = s.objs.bhs.size();
	auto partn = s.objs.parts.size();

	// TODO:
	// Pit�isk� bhole/particlen olla jokin luokka, joka osaa ilmoittaa montako 
	// dynaamista muuttujaa sill� on, ja antaa ne tarvittessa flat array -muodossa?
	// Ja mahdollisesti lis�ksi toimia komponentti-tyyliin (accretion-komponentti,
	// PN-correction inducing -komponentti jne)?

	// 
	// Keplerian accelerations for bholes and particles
	VectorXd kep_acc_bhs = VectorXd::Zero(3 * bhn);
	VectorXd kep_acc_parts = VectorXd::Zero(3 * partn+1);
	// Also store all terms of form (r_j - r_i)/|r_j - r_i|^3 needed for TTL
	VectorXd omega_derivs = VectorXd::Zero(3 * bhn);

#if 1
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < bhn; i++) {
		bhole &pi = s.objs.bhs[i];
		for (long j = i+1; j < bhn; j++) {
			bhole &pj = s.objs.bhs[j];
			Vector3d f = rhat_over_r2(pi, pj);
#pragma omp critical
			{
				kep_acc_bhs.segment<3>(3*i) += f*pj.m*C_G;
				kep_acc_bhs.segment<3>(3*j) -= f*pi.m*C_G;
				// Use only black holes for TTL
				omega_derivs.segment<3>(3*i) += f;
				omega_derivs.segment<3>(3*j) -= f;
			}
		}
		for (long j = 0; j < partn; j++) {
			particle &pj = s.objs.parts[j];
			if (pj.state != NORMAL) continue;
			Vector3d f = rhat_over_r2(pi, pj);
#pragma omp critical
			{
				kep_acc_bhs.segment<3>(3*i) += f*pj.m*C_G;
				kep_acc_parts.segment<3>(3*j) -= f*pi.m*C_G;
			}
		}
	}
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < partn; i++) {
		particle &pi = s.objs.parts[i];
		if (pi.state != NORMAL) continue;
		for (long j = i+1; j < partn; j++) {
			particle &pj = s.objs.parts[j];
			if (pj.state != NORMAL) continue;
			Vector3d f = rhat_over_r2(pi, pj);
#pragma omp critical
			{
				kep_acc_parts.segment<3>(3*i) += f*pj.m*C_G;
				kep_acc_parts.segment<3>(3*j) -= f*pi.m*C_G;
			}
		}
	}
#endif

	// 
	// Add disk internal interactions to kepler force, to average
	// them over the step
	// TODO: Internal interactions removed for now

	// TODO: refaktoroi PN-iteraatio:
	// - pit�� olla virhetilanne, jos iteraatio ei konvergoidu (ainakin mustille aukoille)! 
	//		=> pienennet��n aika-askelta, tai laitetaan nollaksi tai jotain

	// We will calculate average velocity v_avg over step size h, starting with 
	// an initial guess v_avg0 = v0 + h/2 * kep_accel, and using this to
	// calculate PN-acceleration pn_accel0. 
	// Then we find v_avg1 = v0 + h/2 * (kep_accel + pn_accel0), and use
	// this to calculate pn_accel1 and so on, until pn_accel converges.
	//
	// The check for convergence is comparison of 
	// sum_i |v_avg(n+1)_i - v_avgn_i| / sum_i | v_avg0_i| 
	// i.e. comparing the sums of absolute values of differences in components of v_avg.
	//
	// Spins are updated similarly to velocities, except that there is obviously
	// no Keplerian contribution.

	// Initial average velocities for bholes and particles and initial spins for bholes
	VectorXd avg_vel_bhs = hs/2 * kep_acc_bhs;
	VectorXd avg_vel_parts = hs/2 * kep_acc_parts;
	VectorXd avg_spin_bhs(3 * bhn);
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < bhn; i++) {
		avg_vel_bhs.segment<3>(3*i) += s.objs.bhs[i].v;
		avg_spin_bhs.segment<3>(3*i) = s.objs.bhs[i].s;
	}
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i = 0; i < partn; i++) {
		if (s.objs.parts[i].state != NORMAL) continue;
		avg_vel_parts.segment<3>(3*i) += s.objs.parts[i].v;
	}

	// Initial sums of absolute values
	double avg_vel_bhs_avsum = avg_vel_bhs.array().abs().sum();
	double avg_spin_bhs_avsum = avg_spin_bhs.array().abs().sum();
	double avg_vel_parts_avsum = avg_vel_parts.array().abs().sum();

	// PN-accelerations & spin derivatives
	VectorXd pn_acc_bhs = VectorXd::Zero(3 * bhn);
	VectorXd pn_dspin_bhs = VectorXd::Zero(3 * bhn);
	VectorXd pn_acc_parts = VectorXd::Zero(3 * partn+1);

	// Sums of absolute values of differences
	double diff_vel_bhs = 0;
	double diff_spin_bhs = 0;
	double diff_vel_parts = 0;

	// Start iteration
	int iteration = 0;
	while (true) {
		++iteration;
		// PN-corrections are calculated pairwise, and must be done in relative coordinates
		// TODO: There is room for optimization in speed with memory cost here: r's stay constant,
		// so we could calculate them once, store, and use again
		
		pn_acc_bhs.setZero();
		pn_dspin_bhs.setZero();
		pn_acc_parts.setZero();
		
		VectorXd pn_acc_bhs_locsum = VectorXd::Zero(3 * bhn);
		VectorXd pn_dspin_bhs_locsum = VectorXd::Zero(3 * bhn);
		VectorXd pn_acc_parts_locsum = VectorXd::Zero(3 * partn+1);

		// TODO: For some reason this parallel for causes errors
#pragma omp parallel for firstprivate(pn_acc_bhs_locsum,pn_dspin_bhs_locsum,pn_acc_parts_locsum) if (g_progconf.use_openmp)
		for (long i=0; i < bhn; i++) {
			bhole &pi = s.objs.bhs[i];
			for (long j=i+1; j < bhn; j++) {
				bhole &pj = s.objs.bhs[j];
				double total_mass = pi.m + pj.m;
				Vector3d rij = pj.r - pi.r;
				Vector3d vij = avg_vel_bhs.segment<3>(3*j) - avg_vel_bhs.segment<3>(3*i);
				Vector3d si = avg_spin_bhs.segment<3>(3*i);

				Vector3d pn_acc = pn_terms(C_G, C_c, pi.m, pj.m, rij, vij) 
						+ pn_spin_terms(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				// TODO: Must have spin-spin terms here, now using only chi and spin of pi!
				// TODO: Need to use both components from av_spin_bhs
				Vector3d pn_dspin = pn_sdot(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				//cerr << "pnacc " << pn_acc.transpose().format(eigen_format) << endl;
				//cerr << "pndsp " << pn_dspin.transpose().format(eigen_format) << endl;

#if 1
#pragma omp critical
				{ 
					pn_acc_bhs.segment<3>(3*i) -= pn_acc*pj.m/total_mass;
					pn_acc_bhs.segment<3>(3*j) += pn_acc*pi.m/total_mass;
					pn_dspin_bhs.segment<3>(3*i) += pn_dspin;
				}
#endif
			}
			for (long j=0; j < partn; j++) {
				particle &pj = s.objs.parts[j];
				if (pj.state != NORMAL) continue;
				double total_mass = pi.m + pj.m;
				Vector3d rij = pj.r - pi.r;
				Vector3d vij = avg_vel_parts.segment<3>(3*j) - avg_vel_bhs.segment<3>(3*i);
				Vector3d si = avg_spin_bhs.segment<3>(3*i);

				Vector3d pn_acc = pn_terms(C_G, C_c, pi.m, pj.m, rij, vij) 
					+ pn_spin_terms(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 
				// TODO: Must have spin-spin terms here, now using only chi and spin of pi!
				Vector3d pn_dspin = pn_sdot(C_G, C_c, pi.m, pj.m, pi.chi, s.physical.q, rij, vij, si); 

#if 1
#pragma omp critical 
				{
					pn_acc_bhs.segment<3>(3*i) -= pn_acc*pj.m/total_mass;
					pn_acc_parts.segment<3>(3*j) += pn_acc*pi.m/total_mass;
					pn_dspin_bhs.segment<3>(3*i) += pn_dspin;
				}
#endif
			}
		}
		// NOTE: No PN-correction between particles
		cerr << "pnacc " << pn_acc_bhs.transpose().format(eigen_format) << endl;
		cerr << "pndsp " << pn_dspin_bhs.transpose().format(eigen_format) << endl;
		exit(0);

		// Update average velocities, and store the old values
		VectorXd avg_vel_bhs_old = avg_vel_bhs;
		VectorXd avg_spin_bhs_old = avg_spin_bhs;
		VectorXd avg_vel_parts_old = avg_vel_parts;

		avg_vel_bhs = hs/2*(kep_acc_bhs + pn_acc_bhs);
		avg_spin_bhs = hs/2*pn_dspin_bhs;
		avg_vel_parts = hs/2*(kep_acc_parts + pn_acc_parts);

#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < bhn; i++) {
			avg_vel_bhs.segment<3>(3*i) += s.objs.bhs[i].v;
			avg_spin_bhs.segment<3>(3*i) += s.objs.bhs[i].s;
		}
#pragma omp parallel for if (g_progconf.use_openmp)
		for (long i = 0; i < partn; i++) {
			if (s.objs.parts[i].state != NORMAL) continue;
			avg_vel_parts.segment<3>(3*i) += s.objs.parts[i].v;
		}

		// Now check for convergence
		double eps = g_progconf.pn_epsilon;
		double diff_vel_bhs_old = diff_vel_bhs;
		double diff_spin_bhs_old = diff_spin_bhs;
		double diff_vel_parts_old = diff_vel_parts;
		diff_vel_bhs = (avg_vel_bhs - avg_vel_bhs_old).array().abs().sum();
		diff_spin_bhs = (avg_spin_bhs - avg_spin_bhs_old).array().abs().sum();
		diff_vel_parts = (avg_vel_parts - avg_vel_parts_old).array().abs().sum();

		//cerr << "kepf " << (kep_acc_bhs.segment<3>(3)-kep_acc_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "dv   " << (pn_acc_bhs.segment<3>(3)-pn_acc_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "va   " << (avg_vel_bhs.segment<3>(3)-avg_vel_bhs.segment<3>(0)).transpose() << endl;
		//cerr << "dsp  " << pn_dspin_bhs.transpose() << endl;

		// First, divergence check
		if (iteration > 1 && (check_pn_divergence("BH velocity", iteration, diff_vel_bhs_old, diff_vel_bhs)
			|| check_pn_divergence("BH spin", iteration, diff_spin_bhs_old, diff_spin_bhs)
			|| check_pn_divergence("Particle velocity", iteration, diff_vel_parts_old, diff_vel_parts))) {
			// We have divergence. We should lower timestep!
			// TODO: Abort and lower timestep
			break;
		}

		// Check convergence conditions
		bool conv_vel_bhs = diff_vel_bhs <= eps * avg_vel_bhs_avsum;
		bool conv_spin_bhs = diff_spin_bhs <= eps * avg_spin_bhs_avsum;
		bool conv_vel_parts = diff_vel_parts <= eps * avg_vel_parts_avsum;

		if (conv_vel_bhs && conv_spin_bhs && conv_vel_parts) {
			// Convergence
			break;
		}
		else if (iteration >= g_progconf.pn_max_it) {
			// Ran out of iterations. Print the parts that failed to converge
			if (!conv_vel_bhs)
				g_debug_stream << "PN iteration: BH velocity not converged in " 
					<< iteration << " iterations. Residual: " << diff_vel_bhs 
					<< " Tolerance: " << eps * avg_vel_bhs_avsum << endl;
			if (!conv_spin_bhs)
				g_debug_stream << "PN iteration: BH spin not converged in " 
					<< iteration << " iterations. Residual: " << diff_spin_bhs 
					<< " Tolerance: " << eps * avg_spin_bhs_avsum << endl;
			if (!conv_vel_parts)
				g_debug_stream << "PN iteration: Particle velocity not converged in " 
					<< iteration << " iterations. Residual: " << diff_vel_parts 
					<< " Tolerance: " << eps * avg_vel_parts_avsum << endl;

			// TODO: Should abort substeps, lower timestep and try again?
			break;
		}
	}

	// Make extra sure that there are no infinities or nans around
	// TODO: Do these work?
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < pn_acc_bhs.size(); i++) {
		if (!custom_isfinite(pn_acc_bhs[i])) {
#pragma omp critical
			{
				g_debug_stream << "PN iteration: pn_acc_bhs["<<i<<"] NOT FINITE!" << endl;
				pn_acc_bhs[i] = 0.0;
			}
		}
	}

	// Now, update velocities & spins to reflect state at end of step
	kep_acc_bhs += pn_acc_bhs;
	kep_acc_parts += pn_acc_parts;

#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < bhn; i++) {
		s.objs.bhs[i].v += hs * kep_acc_bhs.segment<3>(3*i);
		s.objs.bhs[i].s += hs * pn_dspin_bhs.segment<3>(3*i);
	}
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < partn; i++) {
		if (s.objs.parts[i].state != NORMAL) continue;
		s.objs.parts[i].v += hs * kep_acc_parts.segment<3>(3*i);
	}

	// Calculate change in GR energy
	double egdot = 0;
	for (long i=0; i < bhn; i++) {
		egdot += s.objs.bhs[i].m / s.objs.m_tot_bhs * 
			avg_vel_bhs.segment<3>(3*i).dot(pn_acc_bhs.segment<3>(3*i));
	}
	s.constants.E_gr += hs * egdot;

	// Calculate change in TTL W-value
	double wdot = 0;
#pragma omp parallel for if (g_progconf.use_openmp)
	for (long i=0; i < bhn; i++) 
		wdot += avg_vel_bhs.segment<3>(3*i).dot(omega_derivs.segment<3>(3*i));

	s.integrator_state.ttlw += hs * wdot;

	//cerr << "rij " << (s.objs.bhs[1].r - s.objs.bhs[0].r).transpose() << endl;
	//cerr << "vij " << (avg_vel_bhs.segment<3>(3)-avg_vel_bhs.segment<3>(0)).transpose() << endl;
	//cerr << "oders " << omega_derivs.transpose() << endl;
	//cerr << "wdot " << wdot << " egdot " << egdot << endl;
	//cerr << "hs " << hs << endl;
	//exit(0);
}
#endif