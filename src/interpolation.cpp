#include <cstdlib>
#include <cstring>
#include <cstdio>
#include "interpolation.hpp"

using namespace std;

void fh_allocate(struct fh_precalc *pc, int n_) {
    int n = n_-1;
    pc->n = n;
    pc->w = new double[n+1];
    pc->w_f = new double[n+1];
    pc->xvals = new double[n+1];
}

void fh_reassign(struct fh_precalc *pc, double *xv, double *fv) {
    int n = pc->n;
    memcpy(pc->xvals, xv, (n+1)*sizeof(double));
    for (int k=0; k <= n; k++) {
        pc->w[k] = 0;
        int jk_min = (k-FH_D > 0 ? k-FH_D : 0);
        int jk_max = (k < n-FH_D ? k : n-FH_D);
        for (int i=jk_min; i <= jk_max; i++) {
            double prod = 1;
            for (int j=i; j <= i+FH_D; j++) {
                if (j == k) continue;
                //fprintf(stderr, "fh_precalculate: k %d i %d j %d\n", k, i, j);
                prod *= 1/(xv[k] - xv[j]);
            }
            pc->w[k] += (i % 2 == 0 ? 1 : -1) * prod;
            pc->w_f[k] = pc->w[k] * fv[k];
        }
    }
}

void fh_precalculate(struct fh_precalc *pc, double *xv, double *fv, int n_) {
	fh_allocate(pc, n_);
	fh_reassign(pc, xv, fv);
}

void fh_free(fh_precalc *pc) {
    delete[] pc->w;
    delete[] pc->w_f;
    delete[] pc->xvals;
}

double fh_interp(fh_precalc *pc, double x) {
    double num = 0, denom = 0;
    for (int i=0; i <= pc->n; i++) {
        double dx = 1/(x - pc->xvals[i]);
        num += pc->w_f[i] * dx;
        denom += pc->w[i] * dx;
    }
    return num/denom;
}

void neville_precalculate(neville_precalc *pc, double *xs, double *fs, int n_) {
    int n = n_-1;
    pc->n = n;
    pc->xs = new double[n+1];
    memcpy(pc->xs, xs, (n+1)*sizeof(double));
    pc->p = new double*[n+1];
    for (int i=0; i <= n; i++) {
        pc->p[i] = new double[n+1];
        memset(pc->p[i], 0, (n+1)*sizeof(double));
        pc->p[i][i] = fs[i];
        fprintf(stderr, "neville_precalculate: i %d xi %g pii %g\n", 
                i, pc->xs[i], pc->p[i][i]);
    }
}

void neville_free(neville_precalc *pc) {
    delete[] pc->xs;
    for (int i=0; i < pc->n+1; i++)
        delete[] pc->p[i];
    delete pc->p;
}

double neville_interp(neville_precalc *pc, double x) {
    int n = pc->n;
#if 0
    {
        for (int i=0; i <= n; i++) {
            for (int j=0; j <= n; j++)
                printf("%24.18g ", pc->p[i][j]);
            printf("\n");
        }
    }
#endif
    for (int k=1; k <= n; k++) {
        for (int l=1; l <= n+1-k; l++) {
            int i = l-1, j = i+k;
            //fprintf(stderr, "neville_interp: k %d i %d j %d\n", k, i, j);
            pc->p[i][j] = ((pc->xs[j] - x) * pc->p[i][j-1] 
                    + (x - pc->xs[i])*pc->p[i+1][j]) / (pc->xs[j] - pc->xs[i]);
        }
    }
#if 0
    {
        for (int i=0; i <= n; i++) {
            for (int j=0; j <= n; j++)
                printf("%24.18g ", pc->p[i][j]);
            printf("\n");
        }
    }
#endif
    return pc->p[0][n];
}

double lin_interp(double x, double x1, double x2, double y1, double y2) {
	return (y2-y1)/(x2-x1)*(x-x1) + y1;
}



#if 0
#include <cstdio>
#include <cmath>


int main() {
#if 0
    int n = 5;
    double xvals[] = { 0.1, 0.2, 0.3, 0.4, 0.5 };
    double k = 8;
    double fvals[] = { 
        sin(k*0.1), sin(k*0.2), sin(k*0.3), sin(k*0.4), sin(k*0.5),
    };

    fh_precalc pc;
    fh_precalculate(&pc, xvals, fvals, n);
    neville_precalc nc;
    neville_precalculate(&nc, xvals, fvals, n);
    printf("x interp true diff reldiff\n");
    printf("Interpolating sin() with fh and Neville\n");
    for (int i=0; i < 100; i++) {
        double x = 0.05 + i/90.0;
        double xfh = fh_interp(&pc, x);
        double xnv = neville_interp(&nc, x);
        double sx = sin(k*x);
        printf("%24.18g %24.18g %24.18g %24.18g\n", x, sx, xfh, xnv);
    }
    fh_free(&pc);
    neville_free(&nc);
    return 0;
#endif
    int n = 5;
    double xvals[] = { -2, -1, 0, 1, 2 };
    double fvals[] = { 1, 2, -1, 0, 1};
    fh_precalc pc;
    fh_precalculate(&pc, xvals, fvals, n);
    neville_precalc nc;
    neville_precalculate(&nc, xvals, fvals, n);
    printf("x interp true diff reldiff\n");
    printf("Interpolating sin() with fh and Neville\n");
    for (int i=-205; i <= 205; i++) {
        double x = i/100.0;
        double xfh = fh_interp(&pc, x);
        double xnv = neville_interp(&nc, x);
        double sx = 0;
        printf("%24.18g %24.18g %24.18g %24.18g\n", x, sx, xfh, xnv);
    }
    fh_free(&pc);
    neville_free(&nc);
    return 0;
}
#endif

