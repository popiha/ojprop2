ojprop -- A compact binary propagator for a binary black hole model,
with OJ 287 in mind

Features: 
- Post--Newtonian approximation of the binary vector field
- Geometric propagator: A modified vector field according to Time
  Transformed Leapfrog scheme + symplectic leapfrog propagator

