#ifndef SDLMAIN_HPP
#define SDLMAIN_HPP

// Init SDL OpenGL screen
void sdl_init();

// Update graphics
void sdl_graphics_loop();

#endif // sdlmain.hpp
