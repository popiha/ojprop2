#include <cstdlib>
#include <cstring>
#include <cstdio>

using namespace std;

static const int FH_D = 3;

struct fh_precalc {
    int n;
    double *xvals;
    double *w;
    double *w_f;
};

struct neville_precalc {
    int n;
    double *xs;
    double **p;
};


// Order of the interpolation polynomial is n, which requires n+1 data points.
// Or in other words, for n data points, the maximal order is n-1.

void fh_precalculate(fh_precalc *pc, double *xv, double *fv, int n_); 
void neville_precalculate(neville_precalc *pc, double *xs, double *fs, int n_); 

// Reuse precalc-buffer, must have same n
void fh_reassign(fh_precalc *pc, double *xv, double *fv);

void fh_free(fh_precalc *pc); 
void neville_free(neville_precalc *pc); 

double fh_interp(fh_precalc *pc, double x); 
double neville_interp(neville_precalc *pc, double x); 

double lin_interp(double x, double x1, double x2, double y1, double y2);


