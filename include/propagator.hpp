#ifndef PROPAGATOR_HPP
#define PROPAGATOR_HPP

#include "conf.hpp"
#include "system.hpp"
#include "grid.hpp"

// Propagate the state the given timestep forward, using the given
// configuration
void propagate(State &s, double timestep);

// Subfunctions for propagating disk and binary separately
void bin_propagate(State &s, double timestep); 
void disk_propagate(State &s, double timestep);

// Pointers to TTL functions Omega(r) and 
// (grad Omega(r)) dot v
typedef double (*omega_fp)(const State &s);
typedef double (*deltaw_fp)(const State &s, Vector3d v_avg);

// Omega(r) using binary distances
double omega(const State &s);

// Calculate forces from disk interaction
//void disk_interaction_f(State &s, VectorXd &res);
//void disk_interaction_f(const disk &d, MillerGrid &g, const disk_parameters &m, VectorXd &dv, const Vector3d &roff, const Vector3d &voff);

// Update positions & time and velocities & TTL-w
void rleap(State &s, const double step);
void vleap(State &s, const double step);

#endif // propagator.hpp
