#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "conf.hpp"
#include "misc.hpp"

#include <Eigen/Dense>
using namespace Eigen;

#include <string>
#include <vector>
using namespace std;


// Physical state vector plus nonphysical auxiliary variables for
// propagator use.

// Binary structure
struct bhole {
	// Dynamical
	Vector3d r;	// CM position
	Vector3d v;	// CM velocity
	Vector3d s;	// Direction of spin in Euclidean coordinates, unit vector

	// Rest
	ULONG id;
	double m;	// Mass
	double chi;	// Magnitude of spin
	double acc_r;	// Accretion radius

	// Serialization to/from stream
	void serialize_to_stream(ostream &s) const;
	void serialize_from_stream(istream &s);
};

// Particle states
enum ParticleState {
	NORMAL = 0,
	ACCRETED = 1,
	ESCAPED = 2
};

struct particle {
	// Dynamical 
	Vector3d r;	
	Vector3d v;

	// Rest
	ParticleState state;
	bool is_massless;
	ULONG id;
	double m;

	// Serialization to/from stream
	void serialize_to_stream(ostream &s) const;
	void serialize_from_stream(istream &s);
};

struct consts {
	// Constants of motion
	double E_kep;	// keplerian orbital energy
	double E_gr;	// GR contribution (not really a constant)
	Vector3d h;	// angular momentum
};


class State {
	public:
	// Load initial conditions from a file.
	void load_from_file(char *filename);

	// Physical quantities of black holes and particles
	struct {
		// Black holes
		double m_tot_bhs;		// total mass in bholes
		int n_dyn_bh = 9; // number of dynamical quantities in a bhole
		std::vector<bhole> bhs;
	
		// Particles
		ULONG first_massless_part;		// index of first massless particle
		double m_tot_parts;		// total mass in particles
		int n_dyn_part = 6; // number of dynamical quantities in a bhole
		std::vector<particle> parts;
	} objs;

	// Rest of the global physical quantitites
	struct {
		double t;	// Local (unredshifted) time in internal units
		double z;	// Redshift factor
		double q;	// Quadrupole term
	} physical; 

	// Constants of motion
	consts constants, initial_constants;

	// Integrator state
	struct {
		double ttlw;	// Time transformation variable
	} integrator_state;

	// Statistics. Accumulated until next output.
	struct {
		// Number of accreted particles for each black hole
		vector<ULONG> accretion_count;
		// Total number of accreted particles
		ULONG accretion_total;

		// Accrete a particle to black hole i
		void accrete_particle(int i) {
			accretion_count[i] += 1;
			accretion_total++;
		}

		// Inclination escape data
		ULONG inclination_escape_count;
		// Total number of escaped particles
		ULONG escape_total;

		// Orbital element statistics of the grid particles:
		//		semi-major axis, eccentricity, inclination, longitude of ascending
		//		node, argument of pericenter, mean anomaly at epoch
		// for each of these, compute:
		//		mean, min, max, stddev
		double orbital_elements[6][4];

	} stats;

	// Init stat counters
	void init_accumulators() { 
		stats.accretion_count.resize(objs.bhs.size(), 0);
		stats.accretion_total = 0;
		stats.inclination_escape_count = 0;
	}
	// Clear only those accumulators that should be zeroed before an output
	void clear_accumulators() { 
		for (int i=0; i < objs.bhs.size(); i++)
			stats.accretion_count[i] = 0;
		stats.inclination_escape_count = 0;
	}


	//
	// Member functions

	
	// Convert state variables to astronomical system of solar mass,
	// Astronomical Unit and Julian year
	double physical_time() const;

	// Update constants of motion
	void update_constants(consts &c);

	// Update orbital element statistics
	void update_element_stats();

	// Convert state to strings, either with internal or physical
	// representation
	std::string bholes_to_string() const;
	std::string parts_to_string() const;
	std::string stat_accumulators_to_string() const;
	std::string diagnostics_to_string() const;

	// Calculate disk radial statistics and return them as a string
	std::string disk_radstats_to_string();

	// Return disk orbital element statistics as a string
	std::string disk_oe_stats_to_string() const;
};


#endif // system.hpp 
