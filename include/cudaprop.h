#pragma once
 
#pragma warning(push)
#pragma warning(disable:4996)
#include "thrust/device_vector.h"
#pragma warning(pop)
 
class Hello
{
private:
    thrust::device_vector<unsigned long> m_data;
 
public:
    Hello(const thrust::host_vector<unsigned long>& data);
    unsigned long Sum();
    unsigned long Max();
};

#if 0
struct cuda_kepler_force_func : public thrust::unary_function<Vector3d, Vector3d> {
	Vector3d rcm1, rcm2;
	double m1, m2;
	double G;
	cuda_kepler_force_func(Vector3d _rcm1, Vector3d _rcm2, double _m1, double _m2, double _G) 
		: rcm1(_rcm1), rcm2(_rcm2), m1(_m1), m2(_m2), G(_G) {}
	__host__ __device__ Vector3d operator()(Vector3d r) {
		Vector3d r1 = r - rcm1;
		Vector3d r2 = r - rcm2;
		double d1 = r1.norm();
		double d2 = r2.norm();
		Vector3d dr1 = r1/(d1*d1*d1);
		Vector3d dr2 = r2/(d2*d2*d2);

		Vector3d acc = -G*( m1*dr1 + m2*dr2 );
		return acc;
	}
};
#endif