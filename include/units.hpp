#ifndef UNITS_HPP
#define UNITS_HPP

#include <cmath>

// The internal units are as follows:
// unit of time: 	
// 	Julian year [a] of 365.25 days of 86400 seconds each
// unit of length: 	
// 	Astronomical Unit [AU] of 149 597 870 691 meters
// unit of weigth:
// 	Solar mass [Ms] of 1.98892e30 kg
// 
// Resulting derived constants:
// G = 39.4905095725 AU^3/(a^2 Ms)
// c = 63241.07708809 AU/a

extern const double YEAR_IN_SECONDS;
extern const double AU_IN_METERS;
extern const double SOLAR_MASS_IN_KILOGRAMS;

// Physical constants in Julian Year / AU / Solar mass -system
extern const double C_IAU_G;
extern const double C_IAU_c;
extern const double C_IAU_G_PER_c2;

#if 0
// Physical constants in internal units
extern double C_G;
extern double C_c;
#endif

// XXX: Moved to a system where G = 1 _always_
const double C_G = 1.0;
// C cannot be const, since we must adjust it to conform to Mikkola/Valtonen code 
// if compatibility options are specified
// double C_c = C_IAU_c / std::sqrt(C_IAU_G);
extern double C_c;

#endif // units.hpp

