#ifndef MISC_HPP
#define MISC_HPP

#ifndef M_PI
//#define M_PI    3.14159265358979323846264338327950288   /* pi */
const double M_PI = 3.14159265358979323846264338327950288;   /* pi */
#endif

// typedefs for data types
typedef unsigned long ULONG;
typedef unsigned int UINT;

#include <vector>
#include <string>
#include <boost/lexical_cast.hpp>

#include <Eigen/Dense>
using namespace Eigen;

// Miscellaneous utility functions

// Convert a (numeric) type to string via boost's lexical_cast
template <class T>
std::string stringify(T &num) {
	return boost::lexical_cast<std::string>(num);
}

// Coordinate conversions

// Position: cartesian to cylindrical
Vector3d pos_cart2cyl(Vector3d &r);

// Any vector vcart/vlvlh: To and back from LVLH frame at rcart (radial, angular and axial components, c.f. cyl. coords)
Vector3d cart2lvlh(const Vector3d &rcart, const Vector3d &vcart);
Vector3d lvlh2cart(const Vector3d &rcart, const Vector3d &vlvlh);

/* calculate standard orbital elements: 
 * (semi-major axis, eccentricity, inclination, longitude of ascending
 * node, argument of pericenter, mean anomaly at epoch)
 * from position, velocity and standard gravitational parameter */
void rv2oe_gp(double mu, const Vector3d &r, const Vector3d &v, double *res);

/* as above, with mu = 1 */
void rv2oe(const Vector3d &r, const Vector3d &v, double *res);

/* calculate position and velocity from orbital elements */
void oe2rv_gp(double mu, const double *elements, Vector3d &r, Vector3d &v);
/* as above, with mu = 1 */
void oe2rv(const double *elements, Vector3d &r, Vector3d &v);

/* solve elliptic Kepler's equation for eccentric anomaly, and return it
 * in *res. returns 0 on convergence, -1 on non-convergence */
int solve_ekepler(double tol, int maxit, double ecc, double M, double *res);

// Calculate basic statistics: mean, stddev, min, max
void basic_stats(const double *values, const int n, double *out);
void basic_stats(const std::vector<double> &values, double *out);

// Functions for checking Eigen Vectors/Matrices for pathological floats

#if 0
template<typename Derived>
bool isnotnan(const ArrayBase<Derived>& x);
template<typename Derived>
bool isfinite(const ArrayBase<Derived>& x);
#endif

bool custom_isnotnan(const double x);
bool custom_isfinite(const double x);

// Signum
#if 0
template <typename T> inline constexpr
int signum(T x, std::false_type is_signed) {
    return T(0) < x;
}

template <typename T> inline constexpr
int signum(T x, std::true_type is_signed) {
    return (T(0) < x) - (x < T(0));
}

template <typename T> inline constexpr
int signum(T x) {
    return signum(x, std::is_signed<T>());
}
#endif

// Platform independent /dev/null
extern std::ostream cnull;
extern std::wostream wcnull;

#endif // misc.hpp
