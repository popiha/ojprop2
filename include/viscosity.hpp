#ifndef VISCOSITY_HPP
#define VISCOSITY_HPP

// A table of dynamic viscosities (nu) calculated from the table in Lehto & Valtonen (1996),
// assuming keplerian velocity profile and sigma_rphi = 1/3 b T^4
const int VISCOSITY_POINTS = 11;
extern double viscosity_table[2][VISCOSITY_POINTS];

// Precalculate viscosity interpolation tables
void viscosity_precalc();

// Calculate dynamic viscosity at radius r by interpolation
double viscosity_at(double r);

#endif 