#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include <iostream>
#include <fstream>

#include "conf.hpp"
#include "system.hpp"
//#include "mpi.hpp"

// The configuration variable
extern Configuration g_progconf;

// physical state
extern State g_state;

// mpi data
//extern MPI_data g_mpi_data;

// output streams for orbit output, disk cut output, debug and
// such
void open_streams(); 
void close_streams(); 
void flush_streams(); 

extern std::ofstream g_debug_stream;
extern std::ofstream g_orbit_stream;
extern std::ofstream g_cut_stream;
extern std::ofstream g_disk_stream;
extern std::ofstream g_accretion_stream;
extern std::ofstream g_particle_stream;
extern std::ofstream g_stat_stream;
extern std::ofstream g_element_stream;
extern std::ofstream g_diag_stream;

// Debug print objects to given stream
void debug_print(FILE *stream, const char *preamble, const int &v); 
void debug_print(FILE *stream, const char *preamble, const double &v); 
void debug_print(FILE *stream, const char *preamble, const Vector3d &v); 

// Debug
#if 0
void db(State &s, int line, char* file);
#endif

// Output format with double precision.
extern Eigen::IOFormat eigen_format;

#endif
