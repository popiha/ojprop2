#ifndef CONF_HPP
#define CONF_HPP

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <string>
#include <set>

class PropertyTreeContainer {
protected:
	boost::property_tree::ptree pt;
public:
	// Get the underlying property tree
	boost::property_tree::ptree& get_ptree() { return pt;}	

	// Update the property tree from values within the class
	virtual void sync_ptree() = 0; //{ }

	// Update class instance with values from property tree
	virtual void sync_instance() = 0; //{ }

	// Convert to string with INFO format representation
	std::string to_string() { 
		std::stringstream ss;
		sync_ptree();
		write_info(ss, pt);
		return ss.str();
	}

	// Load and save from file 
	void load(const std::string &filename) { 
		read_info(filename, pt); 
		sync_instance();
	}
	void save(const std::string &filename) { 
		sync_ptree();
		write_info(filename, pt); 
	}

	// Reset to default values
	virtual void defaults() { }
};

// Propagator configuration
class Configuration : public PropertyTreeContainer {
private:
	typedef PropertyTreeContainer super;
public:
	// Function overrides
	void sync_ptree();
	void sync_instance();
	void defaults();

	// Starting and ending years (Julian, Epoch 2000?)
	double start_year;
	double end_year;

	// Timestep in Keplerian orbital periods
	double max_timestep;
	double min_timestep;

	// Whether to print orbit data and printing interval in Julian years
	bool print_progress;
	double print_interval;

	// Whether to print disk snapshots and printing interval in Julian years
	bool print_disk;
	double disk_interval;

	// Distance of secondary from z=0 plane in AUs, when disk info is dumped
	double disk_print_distance;

	// Is G=1 internally?
	bool unit_G;

	// Should we use Time Transformed Leapfrog (TTL) to scale
	// timesteps?
	bool use_TTL;

	// Use PN-terms for disk-m1 and disk-m2 interaction?
	bool use_disk_m1_PN;
	bool use_disk_m2_PN;

	// Enable viscous disk interaction with a grid?
	bool disk_interaction;

	// Should we output disk snapshots when z=0 (or disk median
	// level, calculated from grid) is crossed?
	bool disk_snapshots;

	// Is accretion to black holes enabled?
	bool disk_accretion;

	// Can particles escape primary disk due to high inclination?
	bool disk_inclination_escape;

	// Do we use accurate Bulirsch-Stoer for the disk?
	bool fast_disk;

	// Use OpenMP parallel regions?
	bool use_openmp;

	// Convergence and iteration limits
	double cut_epsilon, cut_max_it; // Cut finder
	double pn_epsilon, pn_max_it;	// Post-Newt. iteration
	double bs_epsilon;		// Bulirsch-Stoer epsilon

	// Escape radius
	double escape_radius;

	// Enabled compatibility (with Valtonen & Mikkola code) tweaks
	std::set<std::string> tweaks; 

	// Enabled Post-Newtonian terms
	//std::set<std::string> pn_terms; 
	bool PN1, PN2, PN2_5, PN3, PN3_5;
bool PN_spinorbital, PN_quadrupole, PN_spindot;

	// Debugging flags
	std::set<std::string> debug_modules; // Modules where debug is enabled

	// Output settings
	std::string debug_file;		// Debug output
	std::string orbit_file;		// Trajectory data
	std::string cut_file;		// z=0 cuts
	std::string disk_file;		// Disk statistics
	std::string accretion_file;	// Accretion statistics
	std::string particle_file;	// Particle events
	std::string stat_file;		// Statistics
	std::string element_file;	// Orbital elements
	std::string diag_file;	// Propagator diagnostics (Keplerian energy etc.)

	// Check if given compatibility setting is enabled
	bool tweak_enabled(const std::string &term);

	// Check if given debug flag is set
	bool debug_enabled(const std::string &module_name);
};


#endif // conf.hpp
