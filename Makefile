CC=gcc
CXX=g++
RM=rm -f
WARNS=-Wall -Wno-ignored-attributes -Wno-deprecated-declarations
INCLUDES=-I./include -I/usr/include/eigen3 
#OPT=-O2 -march=native
# XXX: for faster performance
OPT=-Ofast -march=native -ffast-math -funroll-loops 
CPPFLAGS=-g $(OPT) $(INCLUDES) $(WARNS) -fopenmp -DEIGEN_NO_DEBUG
LDFLAGS=$(CPPFLAGS)
LDLIBS=-lm

SRCS=src/args.cpp src/bs.cpp src/conf.cpp src/globals.cpp src/grid.cpp src/interpolation.cpp src/main.cpp src/misc.cpp src/pn.cpp src/propagator.cpp src/sdlmain.cpp src/system.cpp src/units.cpp src/viscosity.cpp

OBJS=$(subst .cpp,.o,$(SRCS))

all: ojprop2

bin:
	mkdir bin

ojprop2: bin $(OBJS) Makefile
	$(CXX) $(LDFLAGS) -o bin/ojprop2 $(OBJS) $(LDLIBS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend

include .depend
